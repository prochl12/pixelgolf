void message(String msg){
  fill(0, 169, 0);
  rectMode(CENTER);
  stroke(255);
  rect(width/2, height/2, 350, 80);
  fill(255);
  textFont(font2);
  textAlign(CENTER, CENTER);
  text(msg, width/2, (height/2));
  if(space||lmb){
    state = INGAME;
  }
}

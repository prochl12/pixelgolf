class Club{
  
  float speed, vSpeed;
  int clubType, distance;
  String name;
  
  Club(String tempName, float tempSpeed, float tempVSpeed, int tempDistance, int tempClubType){
    this.name = tempName;
    this.speed = tempSpeed;
    this.vSpeed = tempVSpeed;
    this.distance = tempDistance;
    this.clubType = tempClubType;
  }
  
}

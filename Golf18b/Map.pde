class Map{
  
  String[] mapInfo = new String[5];
  String[] tempTiles;
  String name;
  PVector teeVector = new PVector();
  PVector holeVector = new PVector();
  int par;
  int sizeX, sizeY;
  //tabMap stuff
  float tabMapTileSize, tabMapOffX, tabMapOffY;
  
  Map(String path){
    this.name = path;
    
    //MAP INFO
    this.mapInfo = loadStrings(path+"data.txt");
    
    par = Integer.valueOf(mapInfo[MAP_PAR]);
    holeVector.set(
      Integer.valueOf(mapInfo[MAP_HOLE_X]),
      Integer.valueOf(mapInfo[MAP_HOLE_Y])
    );
    teeVector.set(
      Integer.valueOf(mapInfo[MAP_TEE_X]),
      Integer.valueOf(mapInfo[MAP_TEE_Y])
    );
    
    //TILE INFO
    tempTiles = loadStrings(path+"tiles.txt");
    this.sizeY = this.tempTiles.length;
    this.sizeX = this.tempTiles[0].length();
    tiles = new Tile[sizeX][sizeY];
    for(int i = 0; i<this.sizeX; i++){
      for(int j = 0; j<this.sizeY; j++){
        tiles[i][j] = new Tile(i,j,Integer.parseInt(String.valueOf(this.tempTiles[j].charAt(i))));
      }
    }
    
    //WIND
    wind = new Wind();
    
    //TabMap Calculations
    if((width-20)/(this.sizeX)<(height-20)/(this.sizeY)){
      //println("Mapa je siroka");
      this.tabMapTileSize = (width-20)/(this.sizeX);
      this.tabMapOffX = (width-(tabMapTileSize*sizeX))/2;
      this.tabMapOffY = ((height)-(this.tabMapTileSize*this.sizeY))/2;
    }else{
      //println("Mapa je vysoka");
      this.tabMapTileSize = (height-20)/(this.sizeY);
      this.tabMapOffX = ((width)-(this.tabMapTileSize*this.sizeX))/2;
      this.tabMapOffY = (height-(tabMapTileSize*sizeY))/2;
    }
  }
}

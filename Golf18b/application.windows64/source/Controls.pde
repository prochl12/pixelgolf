//CONTROLS HANDLING
  //KEYBOARD
  void keyPressed(){
    //println(keyCode);
    if (keyCode == LEFT){
      left=true;
    }
    else if (keyCode == RIGHT){
      right=true;
    }
    else if (keyCode == UP){
      up=true;
    }
    else if (keyCode == DOWN){
      down=true;
    }
    else if (keyCode == 32){
      space=true;
    }
    else if (keyCode == 10){
      enter=true;
    }
    else if (keyCode == 9){
      tab=true;
    }else if (key == ESC) {
      esc = true;
      key = 0;
    }
  }
  void keyReleased(){
    if (keyCode == LEFT){
      left=false;
    }
    else if (keyCode == RIGHT){
      right=false;
    }
    else if (keyCode == UP){
      up=false;
    }
    else if (keyCode == DOWN){
      down=false;
    }
    else if (keyCode == 32){
      space=false;
    }
    else if (keyCode == 10){
      enter=false;
    }
    else if (keyCode == 9){
      tab=false;
    }
    esc = false;
  }
  
  
  
  
  
  //MOUSE
  void mousePressed(){
    if (mouseButton == LEFT) {
      lmb=true;
      leftClick();
    } else if (mouseButton == RIGHT) {
      rmb=true;
      rightClick();
    } else {
      mmb=true;
      middleClick();
    }
  }
  void mouseReleased(){
    if (mouseButton == LEFT) {
      lmb=false;
    } else if (mouseButton == RIGHT) {
      rmb=false;
    } else {
      mmb=false;
    }
  }
  
  //LEFT CLICK to AIM
void leftClick(){
  if(state==INGAME){
    if(shotState<3){
      if(shotState==1){
        ball.setAimVector();
      }else if(shotState==2){
        ball.power = swingBar.value;
        ball.hit(clubs[selectedClub]);
      }
      shotState++;
    }
  }
}

//RIGHT CLICK to CANCEL AIM
void rightClick(){
  if(state==INGAME){
    if(shotState==1||shotState==2){
      shotState--;
    }
  }
}

//MIDDLE CLICK
void middleClick(){
  
}


//MOUSE SCROLL
void mouseWheel(MouseEvent event) {
  float e = event.getCount();
  if (e==1.0){
    scrollDn();
  } else if (e==-1.0){
    scrollUp();
  }
}

//function for scroll down
void scrollDn(){
  if(state==INGAME){
    if (selectedClub>0){
      selectedClub--;
    } else {
      selectedClub = clubs.length-1;
    }
  }
  //println("Selected club "+selectedClub+" ("+clubs[selectedClub].name+")");
}

//function for scroll up
void scrollUp(){
  if(state==INGAME){
    if (selectedClub<clubs.length-1){
      selectedClub++;
    } else {
      selectedClub = 0;
    }
  }
  //println("Selected club "+selectedClub+" ("+clubs[selectedClub].name+")");
}

//CP5 Listener

public void controlEvent(ControlEvent theEvent) {
  //println(theEvent.getValue(), state);
  if(state==MAIN){
    if(theEvent.getValue()==1){
      state=HOLESELECT;
    }else if (theEvent.getValue()==2){
      state=COURSESELECT;
    }else if (theEvent.getValue()==3){
      state=PLAYERCONFIG;
    }else if (theEvent.getValue()==4){
      state=MULTIPLAYER;
    }
  }else if(state==HOLESELECT){
    if(theEvent.getValue()==0.5){
      state = MAIN;
    }else if(theEvent.getValue() == 1.5){
      map = loadMap(sketchPath()+"\\data\\maps\\"+courseNames[(int)courseList.getValue()]+"\\"+holeNames[(int)holeList.getValue()]+"\\");
      holeNum = 0;
      state = INGAME;
    }else if(theEvent.getController().getName()=="courses"){
      holeNames = listFolderNames(sketchPath()+"\\data\\maps\\"+courseNames[(int)theEvent.getController().getValue()]);
      holeList.clear();
      holeList.addItems(holeNames);
      holeList.open();
    }else if(theEvent.getController().getName()=="holes"){
      cp5holeSelect.getController("load").setColorBackground(color(0,150,0));
      cp5holeSelect.getController("load").unlock();
    }
  }else if(state==COURSESELECT){
    if(theEvent.getValue()==0.5){
      state = MAIN;
    }else if(theEvent.getValue() == 1.5){
      holeNum = 1;
      map = loadMap(maps[0]);
      state = INGAME;
    }else if(theEvent.getController().getName()=="courses"){
      holeNames = listFolderNames(sketchPath()+"\\data\\maps\\"+courseNames[(int)theEvent.getController().getValue()]);
      maps = new String[holeNames.length];
      strokesField = new int[holeNames.length];
      parsField = new int[holeNames.length];
      cp5courseSelect.getController("load").setColorBackground(color(0,150,0));
      cp5courseSelect.getController("load").unlock();
      for(int i = 0; i<maps.length;  i++){
        maps[i] = sketchPath()+"\\data\\maps\\"+courseNames[(int)courseList.getValue()]+"\\"+holeNames[i]+"\\";
      }
    }
  }else if(state==HOLEDONE){
    if(theEvent.getValue()==0){
      //back to main
      state = MAIN;
    }else if(theEvent.getValue()==1){
      //replay hole
      map = loadMap(sketchPath()+"\\data\\maps\\"+courseNames[(int)courseList.getValue()]+"\\"+holeNames[(int)holeList.getValue()]+"\\");
      state = INGAME;
    }
  }else if(state==COURSEHOLEDONE){
    if(theEvent.getValue()==0){
      //back to main
      state = MAIN;
    }else if(theEvent.getValue()==1){
      //next hole
      holeNum++;
      map = loadMap(maps[holeNum-1]);
      state = INGAME;
    }
  }else if(state==COURSEDONE){
    if(theEvent.getValue()==0){
      //back to main
      state = MAIN;
    }else if(theEvent.getValue()==1){
      //next hole
      for(int i = 0; i < strokesField.length; i++){
        strokesField[i] = 0;
        parsField[i] = 0;
      }
      parSum = 0;
      strokesSum = 0;
      holeNum = 1;
      map = loadMap(maps[0]);
      state = INGAME;
    }
  }else if(state==ESCMENU){
    if(theEvent.getValue()==0){
      //back to game
      state = INGAME;
    }else if(theEvent.getValue()==1){
      //replay hole
      map = loadMap(sketchPath()+"\\data\\maps\\"+courseNames[(int)courseList.getValue()]+"\\"+holeNames[(int)holeList.getValue()]+"\\");
      state = INGAME;
    }else if(theEvent.getValue()==2){
      state = MAIN;
    }
  }else if(state==PLAYERCONFIG){
    if(theEvent.getValue()==256){
      savePlayer();
      state=MAIN;
    }
  }else if(state==MULTIPLAYER){
    if(theEvent.getValue()==0){
      state = MAIN;
    }else if(theEvent.getValue()==1){
      serverConnect(serverIp.getText());
      state = LOBBY;
    }
  }else if(state==LOBBY){
    if(theEvent.getValue()==0){
      state = MULTIPLAYER;
      println("Yolo");
      client.clear();
    }
  }
}

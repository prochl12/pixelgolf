class Ball{
  
  float x,y;
  float lieX, lieY;
  int size;
  float alt;
  int strokes;
  float speed, vSpeed;
  float resist;
  float power;
  boolean flyUp, inHole;
  int flightPhase; //0 static, 1 rise, 2 flight, 3 fade, 4 carry
  int riseDistance;
  float shotDistance;
  float floatDist;
  PVector dir;
  int scopeX, scopeY;
  
  
  Ball(){
    this.alt = 1;
    this.speed = 0;
    this.dir = new PVector();
    this.flyUp = false;
    this.resist = 0.8;
    inHole = false;
  }
  
  
  
  
  void render(){
    //graphics
      stroke(0);
      strokeWeight(1);
      if(alt<=1&&getTileId()==1){
        fill(ballR, ballG, ballB, 100);
      }else{
        fill(ballR, ballG, ballB);
      }
      ellipseMode(CENTER);
      ellipse(
        width/2-viewX,
        height/2-viewY,
        BALL_SIZE*this.alt,
        BALL_SIZE*this.alt
      );
      hole.renderFlag();
    //logic & movement
      //HSPEED
        this.x += (this.speed*this.dir.x)/6;
        this.y += (this.speed*this.dir.y)/6;
        this.speed *= resist;
      //VSPEED
        if(flightPhase == 1){
          this.alt+=this.vSpeed;
          riseDistance++;
          this.vSpeed *=0.95;
          if(this.vSpeed<0.1){
            this.flightPhase = 2;
          }
        } else if (flightPhase == 2){
          riseDistance-=floatDist;
          if(riseDistance<2||floatDist==0){
            flightPhase = 3;
          }
        }else if(flightPhase == 3){
          this.alt-=this.vSpeed/4;
          this.vSpeed /=0.95;
          if(this.alt<1){
            if(this.vSpeed<0.01){
              this.flightPhase = 4;
            }
            this.alt = 1;
            this.vSpeed = 0;
          }
        }
        if(this.isInHole()||inHole){
          println("BALL IN HOLE!!!");
          shotState=5;
          inHole = true;
          if(this.alt>0){
            this.dir.set(ballHole);
            this.dir.normalize();
            this.speed*=1.01;
            this.alt-=0.1;
          }else{
            println("holeNum: "+holeNum);
            if(holeNum==0){
              delay(2000);
              state = HOLEDONE;
            }else{
              strokesField[holeNum-1]=strokes;
              parsField[holeNum-1]=map.par;
              strokesSum += strokes;
              parSum += map.par;
              println(strokesField);
              if(holeNum!=strokesField.length){
                state = COURSEHOLEDONE;
              }else{
                state = COURSEDONE;
              }
            }
          }
        }
        if(shotState==0){
          if(this.getTileId()==GREEN){
            hole.flag = false;
          }else{
            hole.flag = true;
          }
        }
        shotDistance = dist(lieX, lieY, this.x, this.y);
        //WIND
        //println(this.dir);
        if(this.speed>1){
          this.dir.x += wind.dir.x;
          this.dir.y += wind.dir.y;
        }
      //FRICTIONAL RESISTANCE
        if(this.alt>1||flightPhase==1){
          resist = 0.999;
        }else{
          switch(getTileId()){
            case 0:  resist = 0.92; break;    //OB
            case 1:  resist = 0.5;  break;    //WATER
            case 2:  resist = 0.96; break;    //GRASS
            case 3:  resist = 0.98; break;    //GREEN
            case 4:  resist = 0.4;  break;    //BUSH
            case 5:  resist = 0.75; break;    //ROUGH
            case 6:  resist = 0.7;  break;    //BUNKER
            default: resist = 0.9;  break;
          }
          ////println("resist: "+resist+" alt: "+this.alt);
        }
  }
  
  void renderTabMap(){
    stroke(0);
    strokeWeight(1);
    if(alt<=1&&getTileId()==1){
      fill(255, 100);
    }else{
      fill(ballR, ballG, ballB);
    }
    ellipseMode(CENTER);
    ellipse(
      map.tabMapOffX+getTileX()*map.tabMapTileSize+map.tabMapTileSize/2,
      map.tabMapOffY+getTileY()*map.tabMapTileSize+map.tabMapTileSize/2,
      BALL_SIZE/2*this.alt,
      BALL_SIZE/2*this.alt
    );
    //println(map.tabMapOffX+this.x*map.tabMapTileSize,map.tabMapOffY+this.y*map.tabMapTileSize);
  }
  
  
  //RETURN THE COORDS OF THE TILE BALL IS CURRENTLY ON
  int getTileX(){
    int tileX = (int)((this.x)/TILE_SIZE);
    return tileX;
  }
  
  int getTileY(){
    int tileY = (int)((this.y)/TILE_SIZE);
    return tileY;
  }
  
  int getTileId(){
    int tileId=-1;
    try{
      tileId = tiles[this.getTileX()][this.getTileY()].id;
    }catch(Exception e){
    }
    return tileId;
  }
  
  //when in state 1 (aim) and left click, create a vector of the ball aim
  void setAimVector(){
    this.dir.set(width/2-mouseX, height/2-mouseY);
    this.dir.normalize();
  }
  
  //when in state 2 (power) and left click, make the ball move with constant power (yet)
  void hit(Club club){
    shotDistance = 0;
    flightPhase = 1;
    riseDistance = 1;
    flyUp = true;
    if(club.clubType==DRIVER){
      this.floatDist = 20;
    }else{
      this.floatDist = 0;
    }
    power*=resist;
    this.speed = club.speed*(power+0.1);
    this.vSpeed = club.vSpeed*(power+0.1);
    if(strokes==0){
      this.speed*=1.3;
    }else if(clubs[selectedClub].clubType==DRIVER){
      //this.vSpeed *= 0.5;
      //this.speed *= 0.7;
    }
    strokes++;
  }
  
  
  float toPin(){
    float distance;
    distance = dist(this.x, this.y, hole.x*TILE_SIZE, hole.y*TILE_SIZE);
    return distance;
  }
  
  boolean isInHole(){
    if (this.alt<=1.01&&this.speed<15&&this.toPin()<TILE_SIZE/8){
      return true;
    }else{
      return false;
    }
  }
  
  void ob(){
    println("Ball is OB!");
    this.strokes++;
    this.x = this.lieX;
    this.y = this.lieY;
  }
  
  void water(){
    this.ob();
  }

  String rateShot(){
    if(getTileId()==OB){
      return "OB";
    }else if(getTileId()==ROUGH){
      return "ROUGH";
    }else if(getTileId()==BUSH){
      return "BUSH";
    }else if(getTileId()==GRASS){
      return "FAIRWAY";
    }else if(getTileId()==BUNKER){
      return "BUNKER";
    }else if(getTileId()==GREEN){
      if(dist(lieX, lieY, hole.x*TILE_SIZE, hole.y*TILE_SIZE)>500){
        return "NICE ON!";
      }
      return "ON THE GREEN";
    }
    return "";
  }
  
}

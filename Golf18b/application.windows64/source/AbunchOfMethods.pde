//methods for main, just put aside so i avoid spaghetti code

Map loadMap(String path){
  Map tempMap;
  tempMap = new Map(path);
  //BALL PREFERENCES
    ball.x = tempMap.teeVector.x*TILE_SIZE;
    ball.y = tempMap.teeVector.y*TILE_SIZE;
    ball.speed = 0;
    ball.vSpeed = 0;
    ball.alt = 1;
    shotState = 0;
    ball.strokes=0;
    ball.inHole = false;
    selectedClub = 0;
    
    //HOLE PREFERENCES
    hole = new Hole((int)tempMap.holeVector.x, (int)tempMap.holeVector.y);
    hole.distance=dist(tempMap.teeVector.x*TILE_SIZE, tempMap.teeVector.y*TILE_SIZE, tempMap.holeVector.x*TILE_SIZE, tempMap.holeVector.y*TILE_SIZE);
    
    //TEE PREFERENCES
    tee = new Tee((int)tempMap.teeVector.x, (int)tempMap.teeVector.y);
    return tempMap;
}

String wordScore(int strokes, int par){
  if(strokes==1){
    return "Hole in one";
  }
  switch(strokes-par){
    case -3: return "ALBATROSS";
    case -2: return "EAGLE";
    case -1: return "BIRDIE";
    case  0: return "PAR";
    case  1: return "BOGEY";
    case  2: return "DOUBLE BOGEY";
    case  3: return "TRIPLE BOGEY";
    default: 
      if((int)(strokes-par)>0){
        return "+"+String.valueOf((int)strokes-par);
      }else{
        return String.valueOf((int)strokes-par);
      }
  }
}

void drawCourseScoreCard(){
  fill(218,181,121);
  stroke(109, 90, 60);
  strokeWeight(1);
  rectMode(CORNER);
  rect(100, 100, width-200, height-260);
  textFont(font2);
  fill(255);
  textAlign(LEFT, TOP);
  text("SCORECARD", 115, 135);
  textAlign(RIGHT, TOP);
  text("HOLE: "+holeNum+"    RESULT: "+wordScore(ball.strokes,map.par),width-115,135);
  textAlign(RIGHT, CENTER);
  text("HOLE", 200, height/2 - 70);
  text("PAR", 200, height/2 + 64 - 70);
  text("SCORE", 200, height/2 + 128 - 70);
  textAlign(CENTER,CENTER);
  text("SUM" ,200 + ((width-2*220)/strokesField.length+1)*(strokesField.length+1), height/2 - 70);
  text(String.valueOf(parSum) ,200 + ((width-2*220)/strokesField.length+1)*(strokesField.length+1), height/2 + 64 - 70);
  if(parSum>strokesSum){
    text((strokesSum-parSum) ,200 + ((width-2*220)/strokesField.length+1)*(strokesField.length+1), height/2 + 128 - 70);
  }else{
    text("+"+(strokesSum-parSum) ,200 + ((width-2*220)/strokesField.length+1)*(strokesField.length+1), height/2 + 128 - 70);
  }
  for(int i = 0; i<strokesField.length; i++){
    text(i+1 ,200 + ((width-2*220)/(strokesField.length))*(i+1), height/2 - 70);
    text(String.valueOf(parsField[i]) ,200 + ((width-2*220)/(strokesField.length))*(i+1), height/2 + 64 - 70);
    text(String.valueOf(strokesField[i]) ,200 + ((width-2*220)/(strokesField.length))*(i+1), height/2 + 128 - 70);
  }
}

void loadPlayer(){
  String[]plrInfo = new String[4];
  try{
    plrInfo = loadStrings(sketchPath()+"\\data\\player.txt");
    playerName.setText(plrInfo[0]);
    ballR = Integer.valueOf(plrInfo[1]);
    cp5player.getController("red").setValue(ballR);
    ballG = Integer.valueOf(plrInfo[2]);
    cp5player.getController("green").setValue(ballG);
    ballB = Integer.valueOf(plrInfo[3]);
    cp5player.getController("blue").setValue(ballB);
  }catch(NullPointerException e){
    playerName.setText("PLAYER");
    ballR = 255;
    ballG = 255;
    ballB = 255;
  }
}

void savePlayer(){
  saveStrings(sketchPath()+"\\data\\player.txt", new String[]{playerName.getText(), String.valueOf(ballR), String.valueOf(ballG), String.valueOf(ballB)});
}

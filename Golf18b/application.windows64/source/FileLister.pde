String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

String[] listFolderNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    ArrayList<String> namesArray = new ArrayList();
    File files[] = file.listFiles();
    for(File f:files){
      if(f.isDirectory()){
        namesArray.add(f.getName());
      }
    }
    String[] names = new String[namesArray.size()];
    for(int i = 0; i< namesArray.size(); i++){
      names[i] = namesArray.get(i);
    }
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

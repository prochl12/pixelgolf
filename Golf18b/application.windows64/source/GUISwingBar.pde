class SwingBar{
  
  float value;
  boolean up;
  
  SwingBar(){
    this.value = 0.2;
    up = true;
  }
  
  void render(){
    //logics
      if(up){
        if(value<1){
          value/=2.98-2;
        }else{
          value = 0.999;
          up=false;
        }
      }else{
        if(value>0.2){
          value*=2.98-2;
        }else{
          value = 0.2;
          up=true;
        }
      }
    //graphics
      fill(255);
      textFont(font2);
      textAlign(CENTER, BOTTOM);
      text("POWER", width-70, height-260);
      rectMode(CORNER);
      fill(200,50,50);
      noStroke();
      rect(width-120, height-50-200*value, 100, 200*value);
      noFill();
      stroke(255);
      strokeWeight(3);
      rect(width-120, height-250, 100, 200);
  }
}

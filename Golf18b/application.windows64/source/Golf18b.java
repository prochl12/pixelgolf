import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import controlP5.*; 
import processing.net.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Golf18b extends PApplet {

///////////////////////////////////////////////////////
//                   /*GOLF v01*/                    //
//                                                   //
//  /* Ball and tiles + generation */                //
//  /* Ball movement using arrow keys */             //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v02*/                    //
//  /* Mouse control support implemented */          //
//  /* Aim line added*/                              //
//  /* Aiming and powering modes added */            //
//     - click LMB to advance mode, RMB to cancel    //
//  /* Ball's direction vector added */              //
//     - creates automatically when advancing from   //
//       mode 1 (aim) to mode 2 (power)              //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v03*/                    //
//  /* Ball can now be stroked, howewer no club      //
//     types are present yet so now you              //
//     are only able to putt */                      //
//  /* Putting the ball out of map now gives         //
//     a blank color instead of graphic bugs*/       //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v04*/                    //
//  /* Let's try objects of Clubs :D */              //
//  /* Different clubs working!! */                  //
//  /* The WEDGE LIFTING THE BALL!!!!!!              //
//     AYYYYYYYY LMAOOO!!!!! */                      //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v05*/                    //
//  /* Textures updated */                           //
//  /* Ball position can now be altered by           //
//     differing the values of playerX,Y and         //
//     ball.x,y/*                                    //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v06*/                    //
//  /* Added hole class. One hole can be now         //
//     spawned by setting the hole's object          //
//     variables hole.x and hole.y*/                 //
//  /* Reworked ball flight. The ball now moves      //
//     2 times slower but also slowes down           //
//     2 times slower.                               //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v07*/                    //
//  /* The method Ball.isInHole(); returns           //
//     a boolean value saying if the ball's and      //
//     hole's coords do intersect */                 //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v08*/                    //
//  /* Map loading (see map class) although maps     //
//     are not working yet */                        //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v09*/                    //
//  /* Map loading working */                        //
//  /* Ball.toPin() method added */                  //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                  SECOND SEMESTER                  //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v10*/                    //
//  /* GUI implemented */                            //
//  /* TabMap added */                               //
//  /* Added Tee texture, class and Tee itself */    //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v11*/                    //
//  /* GUI tweak */                                  //
//  /* OB */                                         //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v12*/                    //
//  /* Ball movement rework */                       //
//  /* Map loading screen */                         //
//  /* Controllable stroke power! */                 //
//  /* OB tweaked, fixed */                          //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v13*/                    //
//  /* literally no changelog written */             //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v14*/                    //
//  /* Coords fixed!!! */                            //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v15*/                    //
//  /* Club selection GUI */                         //
//  /* Wind & GUI added */                           //
//  /* Flag removed when on green */                 //
//  /* Post-hole scorecard */                        //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v16*/                    //
//  /* Post shot rating */                           //
//  /* Wind strength optimized */                    //
//  /* Hole indicator fixed */                       //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v17*/                    //
//  /* Press ESC to bring up the Pause menu */       //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v18*/                    //
//  /* Whole course can be now played */             //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                  /*GOLF v18b*/                    //
//  /* Let's play around with multiplayer */         //
//                                                   //
//            ///////////////////////////            //
//                                                   //
///////////////////////////////////////////////////////

//TODO LIST:

//PAUSE MENU

//IMPORTS



//CONSTANTS DECLARATION//////////////////////////////////////////
final int SCREEN_WIDTH = 1366;
final int SCREEN_HEIGHT = 768;

final int TILE_SIZE = 64;
final int BALL_SIZE = 10;

//MAP IDs
final int MAP_PAR     = 0;
final int MAP_TEE_X   = 1;
final int MAP_TEE_Y   = 2;
final int MAP_HOLE_X  = 3;
final int MAP_HOLE_Y  = 4;
//TILE IDs
final int OB        = 0;
final int WATER     = 1;
final int GRASS     = 2;
final int GREEN     = 3;
final int BUSH      = 4;
final int ROUGH     = 5;
final int BUNKER    = 6;
//CLUB TYPE IDs
final int DRIVER = 0;
final int WEDGE = 1;
final int PUTTER = 2;

final int OFF = 0;
final int AIM = 1;
final int POWER = 2;
final int MOVING = 3;

//STATES
  final int TITLE = 0;
  final int MAIN = 1;
  final int MAPSELECT = 2;
  final int INGAME = 3;
  final int HOLESELECT = 4;
  final int HOLEDONE = 5;
  final int COURSESELECT = 6;
  final int ESCMENU = 7;
  final int OUT = 8;
  final int MESSAGE = 9;
  final int COURSEHOLEDONE = 10;
  final int COURSEDONE = 11;
  final int PLAYERCONFIG = 12;
  final int MULTIPLAYER = 13;
  final int LOBBY = 14;

//KEYBOARD VARIABLES DECLARATION
boolean left, right, up, down, space, enter, tab, esc;
//MOUSE VARIABLES DECLARATION
boolean lmb, rmb, mmb;

//GAME VARIABLES DECALRATION
ArrayList<PImage> tileImgs = new ArrayList<PImage>();
PImage greenIcon, ballImage;
PImage[] clubImgs = new PImage[3];

//Mutliplayer variables
Client client;

//CP5 VARIABLES
  PFont font1;
  PFont font2;
  
  PImage logo, spaceToStart;
  PImage plrImg, plrBg, plrFg, plrActive;
  float stsh, stsw, stsc;

//CP5 OBJECTS
  ControlFont cfont1;
  ControlFont cfont2;
  
  ScrollableList holeList, courseList, courseList2;
  
  Textfield playerName, serverIp;
  
  ControlP5 cp5title;
  ControlP5 cp5mainmenu;
  ControlP5 cp5ingame;
  ControlP5 cp5courseSelect;
  ControlP5 cp5holeSelect;
  ControlP5 cp5holeDone;
  ControlP5 cp5escMenu;
  ControlP5 cp5courseHoleDone;
  ControlP5 cp5courseDone;
  ControlP5 cp5player;
  ControlP5 cp5multiplayer;
  ControlP5 cp5lobby;

Map map;
Ball ball;
Hole hole;
int holeNum = 0;
int[] strokesField, parsField;
int strokesSum, parSum;
String[] maps;
Tee tee;
Wind wind;
PVector teeHole, ballHole;
Club[] clubs;
Tile[][] tiles;
SwingBar swingBar;
String scoreWord;
int ballR, ballG, ballB;

float viewX, viewY;

String[] courseNames, holeNames;

int state;
boolean showTabMap;

int mapSizeX, mapSizeY;

int shotState;
int selectedClub;

//SETUP////////////////////////////////////////////////////////////
public void setup(){
//sketch setup
  //size(1250,600);
  
  background(0,169,0);
  viewX = 0;
  viewY = 0;
  greenIcon = loadImage("GreenIcon.png");
  ballImage = loadImage("ball.png");
  teeHole = new PVector();
  ballHole = new PVector();
  courseNames = listFolderNames(sketchPath()+"\\data\\maps");
  guiInit();
  loadPlayer();
  swingBar = new SwingBar();
  ball = new Ball();
  state = TITLE;
  //tile images init
  for(int i=0;i<7;i++){
    tileImgs.add(loadImage("tiles\\"+i+".png"));
  }
  //club images init
  for(int i=0;i<3;i++){
    clubImgs[i] = loadImage("clubs\\"+i+".png");
  }
  //clubs init
  selectedClub=0;
  clubs = new Club[]{
    new Club("Driver",     250, 0.3f, 4000, DRIVER),
    new Club("AWD",     200, 0.3f, 3000, DRIVER),
    new Club("Wedge 150",    150, 0.5f,  2800, WEDGE),
    new Club("Wedge 120",    120, 0.5f,  2000, WEDGE),  
    new Club("Wedge 80",     80, 0.5f,  1400, WEDGE),
    new Club("Wedge 50",    80, 0.5f,  1200, WEDGE),
    new Club("Wedge 30",    30, 0.6f,  700, WEDGE),
    new Club("Putter 3",    150, 0.01f, 1200, PUTTER),
    new Club("Putter 2",    50, 0.01f, 480, PUTTER),
    new Club("Putter",      30, 0.01f, 280, PUTTER),
    new Club("MiniPutter",   7, 0.01f, 70, PUTTER),
  };
}
//</setup>

//DRAW/////////////////////////////////////////////////////////////////
public void draw(){
  if(state==OUT){
    message("Out of bounds!!");
  }else if(state==MESSAGE){
    message(ball.rateShot());
  }else if(state==INGAME){
      inGameRender();
  }
    drawGui();
  
}
//</draw>
//methods for main, just put aside so i avoid spaghetti code

public Map loadMap(String path){
  Map tempMap;
  tempMap = new Map(path);
  //BALL PREFERENCES
    ball.x = tempMap.teeVector.x*TILE_SIZE;
    ball.y = tempMap.teeVector.y*TILE_SIZE;
    ball.speed = 0;
    ball.vSpeed = 0;
    ball.alt = 1;
    shotState = 0;
    ball.strokes=0;
    ball.inHole = false;
    selectedClub = 0;
    
    //HOLE PREFERENCES
    hole = new Hole((int)tempMap.holeVector.x, (int)tempMap.holeVector.y);
    hole.distance=dist(tempMap.teeVector.x*TILE_SIZE, tempMap.teeVector.y*TILE_SIZE, tempMap.holeVector.x*TILE_SIZE, tempMap.holeVector.y*TILE_SIZE);
    
    //TEE PREFERENCES
    tee = new Tee((int)tempMap.teeVector.x, (int)tempMap.teeVector.y);
    return tempMap;
}

public String wordScore(int strokes, int par){
  if(strokes==1){
    return "Hole in one";
  }
  switch(strokes-par){
    case -3: return "ALBATROSS";
    case -2: return "EAGLE";
    case -1: return "BIRDIE";
    case  0: return "PAR";
    case  1: return "BOGEY";
    case  2: return "DOUBLE BOGEY";
    case  3: return "TRIPLE BOGEY";
    default: 
      if((int)(strokes-par)>0){
        return "+"+String.valueOf((int)strokes-par);
      }else{
        return String.valueOf((int)strokes-par);
      }
  }
}

public void drawCourseScoreCard(){
  fill(218,181,121);
  stroke(109, 90, 60);
  strokeWeight(1);
  rectMode(CORNER);
  rect(100, 100, width-200, height-260);
  textFont(font2);
  fill(255);
  textAlign(LEFT, TOP);
  text("SCORECARD", 115, 135);
  textAlign(RIGHT, TOP);
  text("HOLE: "+holeNum+"    RESULT: "+wordScore(ball.strokes,map.par),width-115,135);
  textAlign(RIGHT, CENTER);
  text("HOLE", 200, height/2 - 70);
  text("PAR", 200, height/2 + 64 - 70);
  text("SCORE", 200, height/2 + 128 - 70);
  textAlign(CENTER,CENTER);
  text("SUM" ,200 + ((width-2*220)/strokesField.length+1)*(strokesField.length+1), height/2 - 70);
  text(String.valueOf(parSum) ,200 + ((width-2*220)/strokesField.length+1)*(strokesField.length+1), height/2 + 64 - 70);
  if(parSum>strokesSum){
    text((strokesSum-parSum) ,200 + ((width-2*220)/strokesField.length+1)*(strokesField.length+1), height/2 + 128 - 70);
  }else{
    text("+"+(strokesSum-parSum) ,200 + ((width-2*220)/strokesField.length+1)*(strokesField.length+1), height/2 + 128 - 70);
  }
  for(int i = 0; i<strokesField.length; i++){
    text(i+1 ,200 + ((width-2*220)/(strokesField.length))*(i+1), height/2 - 70);
    text(String.valueOf(parsField[i]) ,200 + ((width-2*220)/(strokesField.length))*(i+1), height/2 + 64 - 70);
    text(String.valueOf(strokesField[i]) ,200 + ((width-2*220)/(strokesField.length))*(i+1), height/2 + 128 - 70);
  }
}

public void loadPlayer(){
  String[]plrInfo = new String[4];
  try{
    plrInfo = loadStrings(sketchPath()+"\\data\\player.txt");
    playerName.setText(plrInfo[0]);
    ballR = Integer.valueOf(plrInfo[1]);
    cp5player.getController("red").setValue(ballR);
    ballG = Integer.valueOf(plrInfo[2]);
    cp5player.getController("green").setValue(ballG);
    ballB = Integer.valueOf(plrInfo[3]);
    cp5player.getController("blue").setValue(ballB);
  }catch(NullPointerException e){
    playerName.setText("PLAYER");
    ballR = 255;
    ballG = 255;
    ballB = 255;
  }
}

public void savePlayer(){
  saveStrings(sketchPath()+"\\data\\player.txt", new String[]{playerName.getText(), String.valueOf(ballR), String.valueOf(ballG), String.valueOf(ballB)});
}
class Ball{
  
  float x,y;
  float lieX, lieY;
  int size;
  float alt;
  int strokes;
  float speed, vSpeed;
  float resist;
  float power;
  boolean flyUp, inHole;
  int flightPhase; //0 static, 1 rise, 2 flight, 3 fade, 4 carry
  int riseDistance;
  float shotDistance;
  float floatDist;
  PVector dir;
  int scopeX, scopeY;
  
  
  Ball(){
    this.alt = 1;
    this.speed = 0;
    this.dir = new PVector();
    this.flyUp = false;
    this.resist = 0.8f;
    inHole = false;
  }
  
  
  
  
  public void render(){
    //graphics
      stroke(0);
      strokeWeight(1);
      if(alt<=1&&getTileId()==1){
        fill(ballR, ballG, ballB, 100);
      }else{
        fill(ballR, ballG, ballB);
      }
      ellipseMode(CENTER);
      ellipse(
        width/2-viewX,
        height/2-viewY,
        BALL_SIZE*this.alt,
        BALL_SIZE*this.alt
      );
      hole.renderFlag();
    //logic & movement
      //HSPEED
        this.x += (this.speed*this.dir.x)/6;
        this.y += (this.speed*this.dir.y)/6;
        this.speed *= resist;
      //VSPEED
        if(flightPhase == 1){
          this.alt+=this.vSpeed;
          riseDistance++;
          this.vSpeed *=0.95f;
          if(this.vSpeed<0.1f){
            this.flightPhase = 2;
          }
        } else if (flightPhase == 2){
          riseDistance-=floatDist;
          if(riseDistance<2||floatDist==0){
            flightPhase = 3;
          }
        }else if(flightPhase == 3){
          this.alt-=this.vSpeed/4;
          this.vSpeed /=0.95f;
          if(this.alt<1){
            if(this.vSpeed<0.01f){
              this.flightPhase = 4;
            }
            this.alt = 1;
            this.vSpeed = 0;
          }
        }
        if(this.isInHole()||inHole){
          println("BALL IN HOLE!!!");
          shotState=5;
          inHole = true;
          if(this.alt>0){
            this.dir.set(ballHole);
            this.dir.normalize();
            this.speed*=1.01f;
            this.alt-=0.1f;
          }else{
            println("holeNum: "+holeNum);
            if(holeNum==0){
              delay(2000);
              state = HOLEDONE;
            }else{
              strokesField[holeNum-1]=strokes;
              parsField[holeNum-1]=map.par;
              strokesSum += strokes;
              parSum += map.par;
              println(strokesField);
              if(holeNum!=strokesField.length){
                state = COURSEHOLEDONE;
              }else{
                state = COURSEDONE;
              }
            }
          }
        }
        if(shotState==0){
          if(this.getTileId()==GREEN){
            hole.flag = false;
          }else{
            hole.flag = true;
          }
        }
        shotDistance = dist(lieX, lieY, this.x, this.y);
        //WIND
        //println(this.dir);
        if(this.speed>1){
          this.dir.x += wind.dir.x;
          this.dir.y += wind.dir.y;
        }
      //FRICTIONAL RESISTANCE
        if(this.alt>1||flightPhase==1){
          resist = 0.999f;
        }else{
          switch(getTileId()){
            case 0:  resist = 0.92f; break;    //OB
            case 1:  resist = 0.5f;  break;    //WATER
            case 2:  resist = 0.96f; break;    //GRASS
            case 3:  resist = 0.98f; break;    //GREEN
            case 4:  resist = 0.4f;  break;    //BUSH
            case 5:  resist = 0.75f; break;    //ROUGH
            case 6:  resist = 0.7f;  break;    //BUNKER
            default: resist = 0.9f;  break;
          }
          ////println("resist: "+resist+" alt: "+this.alt);
        }
  }
  
  public void renderTabMap(){
    stroke(0);
    strokeWeight(1);
    if(alt<=1&&getTileId()==1){
      fill(255, 100);
    }else{
      fill(ballR, ballG, ballB);
    }
    ellipseMode(CENTER);
    ellipse(
      map.tabMapOffX+getTileX()*map.tabMapTileSize+map.tabMapTileSize/2,
      map.tabMapOffY+getTileY()*map.tabMapTileSize+map.tabMapTileSize/2,
      BALL_SIZE/2*this.alt,
      BALL_SIZE/2*this.alt
    );
    //println(map.tabMapOffX+this.x*map.tabMapTileSize,map.tabMapOffY+this.y*map.tabMapTileSize);
  }
  
  
  //RETURN THE COORDS OF THE TILE BALL IS CURRENTLY ON
  public int getTileX(){
    int tileX = (int)((this.x)/TILE_SIZE);
    return tileX;
  }
  
  public int getTileY(){
    int tileY = (int)((this.y)/TILE_SIZE);
    return tileY;
  }
  
  public int getTileId(){
    int tileId=-1;
    try{
      tileId = tiles[this.getTileX()][this.getTileY()].id;
    }catch(Exception e){
    }
    return tileId;
  }
  
  //when in state 1 (aim) and left click, create a vector of the ball aim
  public void setAimVector(){
    this.dir.set(width/2-mouseX, height/2-mouseY);
    this.dir.normalize();
  }
  
  //when in state 2 (power) and left click, make the ball move with constant power (yet)
  public void hit(Club club){
    shotDistance = 0;
    flightPhase = 1;
    riseDistance = 1;
    flyUp = true;
    if(club.clubType==DRIVER){
      this.floatDist = 20;
    }else{
      this.floatDist = 0;
    }
    power*=resist;
    this.speed = club.speed*(power+0.1f);
    this.vSpeed = club.vSpeed*(power+0.1f);
    if(strokes==0){
      this.speed*=1.3f;
    }else if(clubs[selectedClub].clubType==DRIVER){
      //this.vSpeed *= 0.5;
      //this.speed *= 0.7;
    }
    strokes++;
  }
  
  
  public float toPin(){
    float distance;
    distance = dist(this.x, this.y, hole.x*TILE_SIZE, hole.y*TILE_SIZE);
    return distance;
  }
  
  public boolean isInHole(){
    if (this.alt<=1.01f&&this.speed<15&&this.toPin()<TILE_SIZE/8){
      return true;
    }else{
      return false;
    }
  }
  
  public void ob(){
    println("Ball is OB!");
    this.strokes++;
    this.x = this.lieX;
    this.y = this.lieY;
  }
  
  public void water(){
    this.ob();
  }

  public String rateShot(){
    if(getTileId()==OB){
      return "OB";
    }else if(getTileId()==ROUGH){
      return "ROUGH";
    }else if(getTileId()==BUSH){
      return "BUSH";
    }else if(getTileId()==GRASS){
      return "FAIRWAY";
    }else if(getTileId()==BUNKER){
      return "BUNKER";
    }else if(getTileId()==GREEN){
      if(dist(lieX, lieY, hole.x*TILE_SIZE, hole.y*TILE_SIZE)>500){
        return "NICE ON!";
      }
      return "ON THE GREEN";
    }
    return "";
  }
  
}
class Club{
  
  float speed, vSpeed;
  int clubType, distance;
  String name;
  
  Club(String tempName, float tempSpeed, float tempVSpeed, int tempDistance, int tempClubType){
    this.name = tempName;
    this.speed = tempSpeed;
    this.vSpeed = tempVSpeed;
    this.distance = tempDistance;
    this.clubType = tempClubType;
  }
  
}
//CONTROLS HANDLING
  //KEYBOARD
  public void keyPressed(){
    //println(keyCode);
    if (keyCode == LEFT){
      left=true;
    }
    else if (keyCode == RIGHT){
      right=true;
    }
    else if (keyCode == UP){
      up=true;
    }
    else if (keyCode == DOWN){
      down=true;
    }
    else if (keyCode == 32){
      space=true;
    }
    else if (keyCode == 10){
      enter=true;
    }
    else if (keyCode == 9){
      tab=true;
    }else if (key == ESC) {
      esc = true;
      key = 0;
    }
  }
  public void keyReleased(){
    if (keyCode == LEFT){
      left=false;
    }
    else if (keyCode == RIGHT){
      right=false;
    }
    else if (keyCode == UP){
      up=false;
    }
    else if (keyCode == DOWN){
      down=false;
    }
    else if (keyCode == 32){
      space=false;
    }
    else if (keyCode == 10){
      enter=false;
    }
    else if (keyCode == 9){
      tab=false;
    }
    esc = false;
  }
  
  
  
  
  
  //MOUSE
  public void mousePressed(){
    if (mouseButton == LEFT) {
      lmb=true;
      leftClick();
    } else if (mouseButton == RIGHT) {
      rmb=true;
      rightClick();
    } else {
      mmb=true;
      middleClick();
    }
  }
  public void mouseReleased(){
    if (mouseButton == LEFT) {
      lmb=false;
    } else if (mouseButton == RIGHT) {
      rmb=false;
    } else {
      mmb=false;
    }
  }
  
  //LEFT CLICK to AIM
public void leftClick(){
  if(state==INGAME){
    if(shotState<3){
      if(shotState==1){
        ball.setAimVector();
      }else if(shotState==2){
        ball.power = swingBar.value;
        ball.hit(clubs[selectedClub]);
      }
      shotState++;
    }
  }
}

//RIGHT CLICK to CANCEL AIM
public void rightClick(){
  if(state==INGAME){
    if(shotState==1||shotState==2){
      shotState--;
    }
  }
}

//MIDDLE CLICK
public void middleClick(){
  
}


//MOUSE SCROLL
public void mouseWheel(MouseEvent event) {
  float e = event.getCount();
  if (e==1.0f){
    scrollDn();
  } else if (e==-1.0f){
    scrollUp();
  }
}

//function for scroll down
public void scrollDn(){
  if(state==INGAME){
    if (selectedClub>0){
      selectedClub--;
    } else {
      selectedClub = clubs.length-1;
    }
  }
  //println("Selected club "+selectedClub+" ("+clubs[selectedClub].name+")");
}

//function for scroll up
public void scrollUp(){
  if(state==INGAME){
    if (selectedClub<clubs.length-1){
      selectedClub++;
    } else {
      selectedClub = 0;
    }
  }
  //println("Selected club "+selectedClub+" ("+clubs[selectedClub].name+")");
}

//CP5 Listener

public void controlEvent(ControlEvent theEvent) {
  //println(theEvent.getValue(), state);
  if(state==MAIN){
    if(theEvent.getValue()==1){
      state=HOLESELECT;
    }else if (theEvent.getValue()==2){
      state=COURSESELECT;
    }else if (theEvent.getValue()==3){
      state=PLAYERCONFIG;
    }else if (theEvent.getValue()==4){
      state=MULTIPLAYER;
    }
  }else if(state==HOLESELECT){
    if(theEvent.getValue()==0.5f){
      state = MAIN;
    }else if(theEvent.getValue() == 1.5f){
      map = loadMap(sketchPath()+"\\data\\maps\\"+courseNames[(int)courseList.getValue()]+"\\"+holeNames[(int)holeList.getValue()]+"\\");
      holeNum = 0;
      state = INGAME;
    }else if(theEvent.getController().getName()=="courses"){
      holeNames = listFolderNames(sketchPath()+"\\data\\maps\\"+courseNames[(int)theEvent.getController().getValue()]);
      holeList.clear();
      holeList.addItems(holeNames);
      holeList.open();
    }else if(theEvent.getController().getName()=="holes"){
      cp5holeSelect.getController("load").setColorBackground(color(0,150,0));
      cp5holeSelect.getController("load").unlock();
    }
  }else if(state==COURSESELECT){
    if(theEvent.getValue()==0.5f){
      state = MAIN;
    }else if(theEvent.getValue() == 1.5f){
      holeNum = 1;
      map = loadMap(maps[0]);
      state = INGAME;
    }else if(theEvent.getController().getName()=="courses"){
      holeNames = listFolderNames(sketchPath()+"\\data\\maps\\"+courseNames[(int)theEvent.getController().getValue()]);
      maps = new String[holeNames.length];
      strokesField = new int[holeNames.length];
      parsField = new int[holeNames.length];
      cp5courseSelect.getController("load").setColorBackground(color(0,150,0));
      cp5courseSelect.getController("load").unlock();
      for(int i = 0; i<maps.length;  i++){
        maps[i] = sketchPath()+"\\data\\maps\\"+courseNames[(int)courseList.getValue()]+"\\"+holeNames[i]+"\\";
      }
    }
  }else if(state==HOLEDONE){
    if(theEvent.getValue()==0){
      //back to main
      state = MAIN;
    }else if(theEvent.getValue()==1){
      //replay hole
      map = loadMap(sketchPath()+"\\data\\maps\\"+courseNames[(int)courseList.getValue()]+"\\"+holeNames[(int)holeList.getValue()]+"\\");
      state = INGAME;
    }
  }else if(state==COURSEHOLEDONE){
    if(theEvent.getValue()==0){
      //back to main
      state = MAIN;
    }else if(theEvent.getValue()==1){
      //next hole
      holeNum++;
      map = loadMap(maps[holeNum-1]);
      state = INGAME;
    }
  }else if(state==COURSEDONE){
    if(theEvent.getValue()==0){
      //back to main
      state = MAIN;
    }else if(theEvent.getValue()==1){
      //next hole
      for(int i = 0; i < strokesField.length; i++){
        strokesField[i] = 0;
        parsField[i] = 0;
      }
      parSum = 0;
      strokesSum = 0;
      holeNum = 1;
      map = loadMap(maps[0]);
      state = INGAME;
    }
  }else if(state==ESCMENU){
    if(theEvent.getValue()==0){
      //back to game
      state = INGAME;
    }else if(theEvent.getValue()==1){
      //replay hole
      map = loadMap(sketchPath()+"\\data\\maps\\"+courseNames[(int)courseList.getValue()]+"\\"+holeNames[(int)holeList.getValue()]+"\\");
      state = INGAME;
    }else if(theEvent.getValue()==2){
      state = MAIN;
    }
  }else if(state==PLAYERCONFIG){
    if(theEvent.getValue()==256){
      savePlayer();
      state=MAIN;
    }
  }else if(state==MULTIPLAYER){
    if(theEvent.getValue()==0){
      state = MAIN;
    }else if(theEvent.getValue()==1){
      serverConnect(serverIp.getText());
      state = LOBBY;
    }
  }else if(state==LOBBY){
    if(theEvent.getValue()==0){
      state = MULTIPLAYER;
      println("Yolo");
      client.clear();
    }
  }
}
public String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

public String[] listFolderNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    ArrayList<String> namesArray = new ArrayList();
    File files[] = file.listFiles();
    for(File f:files){
      if(f.isDirectory()){
        namesArray.add(f.getName());
      }
    }
    String[] names = new String[namesArray.size()];
    for(int i = 0; i< namesArray.size(); i++){
      names[i] = namesArray.get(i);
    }
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}
public void guiInit(){
  //IMAGE STUFF
  logo = loadImage("logo.png");
  spaceToStart = loadImage("spaceToStart.png");
  plrImg = loadImage("plrImg.png");
  plrBg = loadImage("plrBg.png");
  plrFg = loadImage("plrFg.png");
  plrActive = loadImage("plrActive.png");
  stsw = 415;
  stsh = 32;
  stsc = -1;
  
  //FONTS
  font1 = createFont("minecraft.ttf", 64);
  font2 = createFont("glassgauge.ttf", 32);
  cfont1 = new ControlFont(font1);
  cfont2 = new ControlFont(font2);
  
  //CP5s
  //  >TITLE<
  cp5title = new ControlP5(this, font2);
  
  //  >MAIN MENU<
  cp5mainmenu = new ControlP5(this, font2);
  cp5mainmenu.addButton("play hole")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width/2-150, height/2)
    .setSize(300,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5mainmenu.addButton("play course")
    .setBroadcast(false)
    .setValue(2)
    .setBroadcast(true)
    .setPosition(width/2-150, height/2+90)
    .setSize(300,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5mainmenu.addButton("multiplayer")
    .setBroadcast(false)
    .setValue(4)
    .setBroadcast(true)
    .setPosition(width/2-150, height/2+180)
    .setSize(300,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5mainmenu.addButton("player")
    .setBroadcast(false)
    .setValue(3)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setImages(plrBg, plrFg, plrActive)
    .setSize(64,64)
  ;
  
  //  >HOLE SELECT<
  cp5holeSelect = new ControlP5(this, font2);
  courseList = new ScrollableList(cp5holeSelect,"courses")
    .close()
    .setPosition(100, 130)
    .setSize(300, 400)
    .setBarHeight(70)
    .setItemHeight(70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
    .addItems(courseNames)
  ;
  holeList = new ScrollableList(cp5holeSelect,"holes")
    .close()
    .setPosition(width/2-150, 130)
    .setSize(300, 400)
    .setBarHeight(70)
    .setItemHeight(70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5holeSelect.addButton("back")
    .setLabel("<")
    .setBroadcast(false)
    .setValue(0.5f)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(64,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5holeSelect.addButton("load")
    .setLabel("load >")
    .setBroadcast(false)
    .setValue(1.5f)
    .setBroadcast(true)
    .setPosition(width-40-180, height-64-40)
    .setSize(180,64)
    .setColorBackground(color(150, 150))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
    .lock()
  ;
  //  >COURSE SELECT<
  cp5courseSelect = new ControlP5(this, font2);
  courseList2 = new ScrollableList(cp5courseSelect,"courses")
    .setLabel("courses")
    .close()
    .setPosition(100, 130)
    .setSize(300, 400)
    .setBarHeight(70)
    .setItemHeight(70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
    .addItems(courseNames)
  ;
  cp5courseSelect.addButton("back")
    .setLabel("<")
    .setBroadcast(false)
    .setValue(0.5f)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(64,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5courseSelect.addButton("load")
    .setLabel("load >")
    .setBroadcast(false)
    .setValue(1.5f)
    .setBroadcast(true)
    .setPosition(width-40-180, height-64-40)
    .setSize(180,64)
    .setColorBackground(color(150, 150))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
    .lock()
  ;
  
  //  >INGAME<
  cp5ingame = new ControlP5(this, font2);
  
  //  >HOLE DONE<
  cp5holeDone = new ControlP5(this, font2);
  cp5holeDone.addButton("back to menu")
    .setLabel("< menu")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5holeDone.addButton("replay")
    .setLabel("play again >")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width-256-40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  //  >COURSE HOLE DONE<
  cp5courseHoleDone = new ControlP5(this, font2);
  cp5courseHoleDone.addButton("back to menu")
    .setLabel("< menu")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5courseHoleDone.addButton("next hole")
    .setLabel("next hole >")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width-256-40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  //  >COURSE DONE<
  cp5courseDone = new ControlP5(this, font2);
  cp5courseDone.addButton("back to menu")
    .setLabel("< menu")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5courseDone.addButton("play again")
    .setLabel("play again >")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width-256-40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  //  >ESC MENU<
  cp5escMenu = new ControlP5(this, font2);
  cp5escMenu.addButton("back to game")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(width/2-(width/3-30)/2, height/2-65)
    .setSize(width/3-30,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5escMenu.addButton("replay")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width/2-(width/3-30)/2, height/2+15)
    .setSize(width/3-30,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5escMenu.addButton("menu")
    .setBroadcast(false)
    .setValue(2)
    .setBroadcast(true)
    .setPosition(width/2-(width/3-30)/2, height/2+90)
    .setSize(width/3-30,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  
  // >PLAYER CONFIG<
  cp5player = new ControlP5(this, font2);
  playerName = new Textfield(cp5player, "playerName")
     .setLabel("")
     .setPosition(width/8,200)
     .setSize(width/8*6,70)
     .setColor(color(255))
     .setColorCursor(color(255))
     .setColorForeground(color(0,150,0))
     .setColorBackground(color(0,200,0))
     .setValue("player")
     .align(-1, -1, -1, -1)
  ;
  cp5player.addSlider("red")
           .setLabel("")
           .setMax(255)
           .setValue(255)
           .setSize(450, 60)
           .setPosition(width/2-300, 300)
           .setColorBackground(color(150,0,0))
           .setColorForeground(color(255,0,0))
           .setColorActive(color(255,0,0))
  ;
  cp5player.addSlider("green")
           .setLabel("")
           .setMax(255)
           .setValue(255)
           .setSize(450, 60)
           .setPosition(width/2-300, 370)
           .setColorBackground(color(0,150,0))
           .setColorForeground(color(0,255,0))
           .setColorActive(color(0,255,0))
  ;
  cp5player.addSlider("blue")
           .setLabel("")
           .setMax(255)
           .setValue(255)
           .setSize(450, 60)
           .setPosition(width/2-300, 440)
           .setColorBackground(color(0,0,150))
           .setColorForeground(color(0,0,255))
           .setColorActive(color(0,0,255))
  ;
  cp5player.addButton("plrSave")
    .setLabel("< save")
    .setBroadcast(false)
    .setValue(256)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  //  >MULTIPLAYER<
  cp5multiplayer = new ControlP5(this, font2);
  serverIp = new Textfield(cp5multiplayer, "serverIp")
     .setLabel("")
     .setPosition(width/8,200)
     .setSize(width/8*6,70)
     .setColor(color(255))
     .setColorCursor(color(255))
     .setColorForeground(color(0,150,0))
     .setColorBackground(color(0,200,0))
     .setValue("")
  ;
  cp5multiplayer.addButton("connect")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width/2-150, height/2)
    .setSize(300,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5multiplayer.addButton("back")
    .setLabel("<")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(64,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  
  // >PLAYER CONFIG<
  cp5lobby = new ControlP5(this, font2);
  cp5lobby.addButton("disconnect")
    .setLabel("<")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(64,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
}

//--//--//--//--//--//--//--//--//--//--//--//--//--//--//
//--//--//--//--//--//--//--//--//--//--//--//--//--//--//
//--//--//--//--//--//--//--//--//--//--//--//--//--//--//

public void drawGui(){
  //  >TITLE<
    if(state==TITLE){
      if(enter||space){
        state=MAIN;
      }
      background(0,169,0);
      cp5title.show();
      imageMode(CENTER);
      image(logo,width/2,height/2);
      if(stsw>415||stsw<350){
        stsc *= -1;
      }
      stsh += (stsc*0.25f);
      stsw += (stsc*13.97f*0.25f);
      image(spaceToStart, width/2+logo.width/3, height/2+logo.height/2, stsw, stsh);
    }else{
      cp5title.hide();
    }
  
  //  >MAIN MENU<
    if(state==MAIN){
      background(0,169,0);
      cp5mainmenu.show();
      imageMode(CENTER);
      image(logo,width/2,height/4, 350, 214);
      imageMode(CORNER);
      image(plrImg, 40, height-64-40, 64, 64);
    }else{
      cp5mainmenu.hide();
    }
    
  //  >HOLE SELECT<
    if(state==HOLESELECT){
      background(0,169,0);
      cp5holeSelect.show();
      fill(0,150,0);
      noStroke();
      rectMode(CORNER);
      rect(100, 40, 350, 70);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("HOLE SELECT", 105, 45);
    }else{
      cp5holeSelect.hide();
    }
  //  >COURSE SELECT<
    if(state==COURSESELECT){
      background(0,169,0);
      cp5courseSelect.show();
      fill(0,150,0);
      noStroke();
      rectMode(CORNER);
      rect(100, 40, 350, 70);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("COURSE SELECT", 105, 45);
    }else{
      cp5courseSelect.hide();
    }
    
  //  >INGAME<
    if(state==INGAME){
      if(esc){
        state=ESCMENU;
      }
      cp5ingame.show();
      if(shotState==0&&mmb){
        viewX += (float)((mouseX - width/2))/width*64;
        viewY += (float)((mouseY - height/2))/height*64;
      }else{
        viewX = 0;
        viewY = 0;
      }
      ballHole.set(TILE_SIZE*hole.x-ball.x-viewX, TILE_SIZE*hole.y-ball.y-viewY);
      if(abs(ballHole.x)>width/2||abs(ballHole.y)>height/2){
        if(abs(ballHole.x)/(width/2-50)>abs(ballHole.y)/(height/2-50)){
          ballHole.mult((width/2-50)/abs(ballHole.x));
          if(ballHole.x>0){
            //ballHole.x-=50;
          }else{
            //ballHole.x+=50;
          }
        }else{
          ballHole.mult((height/2-50)/abs(ballHole.y));
          if(ballHole.y>0){
            //ballHole.y-=50;
          }else{
            //ballHole.y+=50;
          }
        }
        imageMode(CENTER);
        image(greenIcon, width/2+ballHole.x, height/2+ballHole.y);
      }
      fill(0,150,0);
      noStroke();
     // rect(0, 0, 350, 70, 0, 10, 10, 0);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("TO PIN: "+(float)(round((ball.toPin())*100))/100+"px", 5, 5);
      textAlign(LEFT, BOTTOM);
      text("PAR "+map.mapInfo[0], 5, height-5);
      text((float)((int)(hole.distance*100))/100+"px", 105, height-5);
      text("SHOT DIST: "+(float)((int)(ball.shotDistance*100))/100+"px", 300, height-5);
      textAlign(RIGHT, BOTTOM);
      text("STROKES "+ball.strokes, width-5, height-5);
      imageMode(CORNER);
      image(clubImgs[clubs[selectedClub].clubType], width-128, 0);
      textAlign(RIGHT, CENTER);
      text(clubs[selectedClub].name+" ("+clubs[selectedClub].distance+"px)", width-128-10, 64);
      //render wind
      wind.render();
    }else{
      cp5ingame.hide();
    }
  
  //  >HOLE DONE<
    if(state==HOLEDONE){
      //background(0,169,0,1);
      cp5holeDone.show();
      fill(218,181,121);
      stroke(109, 90, 60);
      strokeWeight(1);
      rect(100, 170, width-200, height-340);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("SCORECARD", 115, 205);
      textAlign(CENTER, CENTER);
      text("PAR: "+map.par+"    STROKES: "+ball.strokes+"    "+wordScore(ball.strokes,map.par),width/2, height/2);
    }else{
      cp5holeDone.hide();
    }
  //  >COURSE HOLE DONE<
    if(state==COURSEHOLEDONE){
      //background(0,169,0,1);
      cp5courseHoleDone.show();
      drawCourseScoreCard();
    }else{
      cp5courseHoleDone.hide();
    }
  //  >COURSE DONE<
    if(state==COURSEDONE){
      cp5courseDone.show();
      drawCourseScoreCard();
    }else{
      cp5courseDone.hide();
    }
  //  >ESC MENU<
    if(state==ESCMENU){
      cp5escMenu.show();
      stroke(255);
      strokeWeight(1);
      fill(0, 169, 0);
      rectMode(CENTER);
      rect(width/2, height/2, width/3, height/3*2);
      fill(255);
      strokeWeight(2);
      textFont(font2);
      textAlign(CENTER, CENTER);
      text("PAUSED",width/2,height/4+10);
    }else{
      cp5escMenu.hide();
    }
    if(state==PLAYERCONFIG){
      ballR = PApplet.parseInt(cp5player.getController("red").getValue());
      ballG = PApplet.parseInt(cp5player.getController("green").getValue());
      ballB = PApplet.parseInt(cp5player.getController("blue").getValue());
      background(0,169,0);
      cp5player.show();
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("PLAYER CONFIG", 105, 45);
      imageMode(CENTER);
      tint(ballR, ballG, ballB);
      image(ballImage, width/2+300-ballImage.width/2, 370);
      noTint();
    }else{
      cp5player.hide();
    }
    //  >MP CONNECT SCREEN<
    if(state==MULTIPLAYER){
      background(0,169,0);
      cp5multiplayer.show();
    }else{
      cp5multiplayer.hide();
    }
    //  >MP LOBBY<
    if(state==LOBBY){
      background(0,169,0);
      cp5lobby.show();
      fill(0,150,0);
      noStroke();
      rectMode(CORNER);
      rect(100, 40, 350, 70);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("LOBBY", 105, 45);
    }else{
      cp5lobby.hide();
    }
}

public void drawAimLine(){
  if (shotState==1||shotState==2){
    strokeWeight(5);
    stroke(0);
    line(ball.scopeX, ball.scopeY, width/2, height/2);
  }
}
class SwingBar{
  
  float value;
  boolean up;
  
  SwingBar(){
    this.value = 0.2f;
    up = true;
  }
  
  public void render(){
    //logics
      if(up){
        if(value<1){
          value/=2.98f-2;
        }else{
          value = 0.999f;
          up=false;
        }
      }else{
        if(value>0.2f){
          value*=2.98f-2;
        }else{
          value = 0.2f;
          up=true;
        }
      }
    //graphics
      fill(255);
      textFont(font2);
      textAlign(CENTER, BOTTOM);
      text("POWER", width-70, height-260);
      rectMode(CORNER);
      fill(200,50,50);
      noStroke();
      rect(width-120, height-50-200*value, 100, 200*value);
      noFill();
      stroke(255);
      strokeWeight(3);
      rect(width-120, height-250, 100, 200);
  }
}
class Hole{
  
  int x,y;
  PImage img, img2;
  boolean flag;
  float distance;
  
  Hole(int tempX, int tempY){
    this.img = loadImage("hole2.png");
    this.img2 = loadImage("flag.png");
    flag = true;
    this.x = tempX;
    this.y = tempY;
  }
  
  public void render(){
    if(this.x!=-1&&this.y!=-1){
      imageMode(CORNER);
      image(this.img,
            width/2+(this.x-0.5f)*TILE_SIZE-ball.x-viewX,
            height/2+(this.y-1.5f)*TILE_SIZE-ball.y-viewY
      );
    }
  }
  
  public void renderFlag(){
    if(this.flag){
      imageMode(CORNER);
      image(this.img2,
            width/2+(this.x-0.5f)*TILE_SIZE-ball.x-viewX,
            height/2+(this.y-1.5f)*TILE_SIZE-ball.y-viewY
      );
    }
  }
  
  public void renderTabMap(){
    if(this.x!=-1&&this.y!=-1){
      imageMode(CORNER);
      image(this.img,
            map.tabMapOffX+(map.holeVector.x*map.tabMapTileSize-map.tabMapTileSize*3),
            map.tabMapOffY+(map.holeVector.y*map.tabMapTileSize-map.tabMapTileSize*6),
            map.tabMapTileSize*3,//w
            map.tabMapTileSize*6//h
      );
      image(this.img2,
            map.tabMapOffX+(map.holeVector.x*map.tabMapTileSize-map.tabMapTileSize*3),
            map.tabMapOffY+(map.holeVector.y*map.tabMapTileSize-map.tabMapTileSize*6),
            map.tabMapTileSize*3,//w
            map.tabMapTileSize*6//h
      );
    }
  }
  
}
public void inGameRender(){
  background(60);
  //logic & movement part
      //arrow movement
      if(up){ball.y--;}
      if(down){ball.y++;}
      if(left){ball.x--;}
      if(right){ball.x++;}
      //mouse aim
      if (shotState==0){
        //not aiming
      }else if (shotState==1){
        //aim with mouse
        ball.scopeX = mouseX;
        ball.scopeY = mouseY;
      } else if (shotState==2){
        ball.lieX = ball.x;
        ball.lieY = ball.y;
        //power with mouse distance
      } else if (shotState==3){
        //stroke the ball
        //when stopped, get ready for next stroke
        if(ball.speed<0.01f){
          ball.speed = 0;
          if(ball.getTileId()<2){
            for(float i = 100; i > 0.000001f; i*=0.9999999f){
            }
            if(ball.getTileId()==1){
              ball.water();
            }else{
              ball.ob();
            }
            state = OUT;
          }else{
            state = MESSAGE;
          }
          ball.flightPhase=0;
          shotState = 0;
        }
      }
    //graphic part
      //render tiles
      for(int i=0;i<map.sizeX;i++){
        for(int j=0;j<map.sizeY;j++){
          tiles[i][j].render();
        }
      }
      //render hole
      hole.render();
      tee.render();
      //render aimline
      drawAimLine();
      //render ball
      ball.render();
      //render swing bar
      if(shotState == 2){
        swingBar.render();
      }
      //tabmap
      if(tab){
        rectMode(CORNER);
        fill(128,128);
        rect(0,0,width,height);
        renderTabMap();
      }
}
class Map{
  
  String[] mapInfo = new String[5];
  String[] tempTiles;
  String name;
  PVector teeVector = new PVector();
  PVector holeVector = new PVector();
  int par;
  int sizeX, sizeY;
  //tabMap stuff
  float tabMapTileSize, tabMapOffX, tabMapOffY;
  
  Map(String path){
    this.name = path;
    
    //MAP INFO
    this.mapInfo = loadStrings(path+"data.txt");
    
    par = Integer.valueOf(mapInfo[MAP_PAR]);
    holeVector.set(
      Integer.valueOf(mapInfo[MAP_HOLE_X]),
      Integer.valueOf(mapInfo[MAP_HOLE_Y])
    );
    teeVector.set(
      Integer.valueOf(mapInfo[MAP_TEE_X]),
      Integer.valueOf(mapInfo[MAP_TEE_Y])
    );
    
    //TILE INFO
    tempTiles = loadStrings(path+"tiles.txt");
    this.sizeY = this.tempTiles.length;
    this.sizeX = this.tempTiles[0].length();
    tiles = new Tile[sizeX][sizeY];
    for(int i = 0; i<this.sizeX; i++){
      for(int j = 0; j<this.sizeY; j++){
        tiles[i][j] = new Tile(i,j,Integer.parseInt(String.valueOf(this.tempTiles[j].charAt(i))));
      }
    }
    
    //WIND
    wind = new Wind();
    
    //TabMap Calculations
    if((width-20)/(this.sizeX)<(height-20)/(this.sizeY)){
      //println("Mapa je siroka");
      this.tabMapTileSize = (width-20)/(this.sizeX);
      this.tabMapOffX = (width-(tabMapTileSize*sizeX))/2;
      this.tabMapOffY = ((height)-(this.tabMapTileSize*this.sizeY))/2;
    }else{
      //println("Mapa je vysoka");
      this.tabMapTileSize = (height-20)/(this.sizeY);
      this.tabMapOffX = ((width)-(this.tabMapTileSize*this.sizeX))/2;
      this.tabMapOffY = (height-(tabMapTileSize*sizeY))/2;
    }
  }
}
public void clientEvent(Client client) {
  String msg = client.readString();
  print(msg);
}

public void serverConnect(String ip){
  client = new Client(this, ip, 12345);
  println("Connected to "+ip);
  client.write("00#"+playerName.getText()+"|");
}
public void message(String msg){
  fill(0, 169, 0);
  rectMode(CENTER);
  stroke(255);
  rect(width/2, height/2, 350, 80);
  fill(255);
  textFont(font2);
  textAlign(CENTER, CENTER);
  text(msg, width/2, (height/2));
  if(space||lmb){
    state = INGAME;
  }
}
class Player{
  
  int id;
  String name;
  
  int x, y, alt;
  int colorR, colorG, colorB;
  
  Player(){
    
  }
  
}
public void renderTabMap(){
  for(int i=0;i<map.sizeX;i++){
    for(int j=0;j<map.sizeY;j++){
      tiles[i][j].renderTabMap();
    }
  }
  ball.renderTabMap();
  hole.renderTabMap();
}
class Tee{
  
  float x1,y1,x2,y2;
  PImage img;
  
  Tee(int tempX, int tempY){
    this.img = loadImage("tee.png");
    teeHole.set(hole.x-tempX, hole.y-tempY);
    teeHole.normalize();
    x1 = tempX + 0.5f*teeHole.x + 1*teeHole.y;
    y1 = tempY + 0.5f*teeHole.y - 1*teeHole.x;
    x2 = tempX + 0.5f*teeHole.x - 1*teeHole.y;
    y2 = tempY + 0.5f*teeHole.y + 1*teeHole.x;
  }
  
  public void render(){
    imageMode(CENTER);
    image(this.img,
          width/2+(this.x1)*TILE_SIZE-ball.x-viewX,
          height/2+(this.y1)*TILE_SIZE-ball.y-viewY
    );
    image(this.img,
          width/2+(this.x2)*TILE_SIZE-ball.x-viewX,
          height/2+(this.y2)*TILE_SIZE-ball.y-viewY
    );
  }
  
}
class Tile{
  
  int id;
  int x,y;
  
  Tile(int tempx, int tempy){
    this.x = tempx;
    this.y = tempy;
    this.id = OB;
  }
  
  Tile(int tempx, int tempy, int tempId){
    this.x = tempx;
    this.y = tempy;
    this.id = tempId;
  }
  
  public void render(){
    imageMode(CORNER);
    image(tileImgs.get(this.id),
          width/2+this.x*TILE_SIZE-ball.x-viewX,
          height/2+(this.y)*TILE_SIZE-ball.y-viewY
    );
  }
  
  public void renderTabMap(){
    imageMode(CORNER);
    image(tileImgs.get(this.id),
          map.tabMapOffX+(this.x*map.tabMapTileSize),//x
          map.tabMapOffY+(this.y*map.tabMapTileSize),//y
          map.tabMapTileSize,//w
          map.tabMapTileSize//h
    );
  }
}
class Wind{
  
  PVector dir, normDir, newDir, arr1, arr2;
  int strength;
  
  Wind(){
    strength = 5000;
    this.dir = new PVector(
      random(-10, 10)/strength,
      random(-10, 10)/strength
    );
    this.normDir = new PVector();
    this.newDir = new PVector();
    this.arr1 = new PVector();
    this.arr2 = new PVector();
  }
  
  public void render(){
    //UPDATE
    if(dir.x>newDir.x+0.01f){
      dir.x-=0.000001f;
    }else if(dir.x<newDir.x-0.01f){
      dir.x+=0.000001f;
    }
    if(dir.y>newDir.y+0.01f){
      dir.y-=0.000001f;
    }else if(dir.y<newDir.y-0.01f){
      dir.y+=0.000001f;
    }
    if(abs(dir.x-newDir.x)<0.0001f&&abs(dir.y-newDir.y)<0.0001f){
      this.update();
    }
    this.dir.normalize(normDir);
    this.arr1.set(normDir);
    arr1.rotate(PI/3*4);
    this.arr2.set(normDir);
    arr2.rotate(PI/3*2);
    //DRAW
    if(state==INGAME){
      stroke(255);
      strokeWeight(5);
      line(50-normDir.x*40, 500-this.normDir.y*40, 50+normDir.x*40, 500+this.normDir.y*40);
      line(50+normDir.x*40, 500+this.normDir.y*40, 50+normDir.x*40+arr1.x*25, 500+this.normDir.y*40+arr1.y*25);
      line(50+normDir.x*40, 500+this.normDir.y*40, 50+normDir.x*40+arr2.x*25, 500+this.normDir.y*40+arr2.y*25);
      //println(normDir);
      textAlign(LEFT, CENTER);
      //println(dist(0,0,dir.x, dir.y));
      text((float)(round((dist(0,0,dir.x, dir.y)*1000000)))/100+"px/h", 100, 500);
    }
  }
  
  public void update(){
    this.newDir = new PVector(
      random(-10, 10)/strength,
      random(-10, 10)/strength
    );
  }
  
}
  public void settings() {  fullScreen(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--hide-stop", "Golf18b" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}

class Tile{
  
  int id;
  int x,y;
  
  Tile(int tempx, int tempy){
    this.x = tempx;
    this.y = tempy;
    this.id = OB;
  }
  
  Tile(int tempx, int tempy, int tempId){
    this.x = tempx;
    this.y = tempy;
    this.id = tempId;
  }
  
  void render(){
    imageMode(CORNER);
    image(tileImgs.get(this.id),
          width/2+this.x*TILE_SIZE-ball.x-viewX,
          height/2+(this.y)*TILE_SIZE-ball.y-viewY
    );
  }
  
  void renderTabMap(){
    imageMode(CORNER);
    image(tileImgs.get(this.id),
          map.tabMapOffX+(this.x*map.tabMapTileSize),//x
          map.tabMapOffY+(this.y*map.tabMapTileSize),//y
          map.tabMapTileSize,//w
          map.tabMapTileSize//h
    );
  }
}

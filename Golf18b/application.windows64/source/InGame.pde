void inGameRender(){
  background(60);
  //logic & movement part
      //arrow movement
      if(up){ball.y--;}
      if(down){ball.y++;}
      if(left){ball.x--;}
      if(right){ball.x++;}
      //mouse aim
      if (shotState==0){
        //not aiming
      }else if (shotState==1){
        //aim with mouse
        ball.scopeX = mouseX;
        ball.scopeY = mouseY;
      } else if (shotState==2){
        ball.lieX = ball.x;
        ball.lieY = ball.y;
        //power with mouse distance
      } else if (shotState==3){
        //stroke the ball
        //when stopped, get ready for next stroke
        if(ball.speed<0.01){
          ball.speed = 0;
          if(ball.getTileId()<2){
            for(float i = 100; i > 0.000001; i*=0.9999999){
            }
            if(ball.getTileId()==1){
              ball.water();
            }else{
              ball.ob();
            }
            state = OUT;
          }else{
            state = MESSAGE;
          }
          ball.flightPhase=0;
          shotState = 0;
        }
      }
    //graphic part
      //render tiles
      for(int i=0;i<map.sizeX;i++){
        for(int j=0;j<map.sizeY;j++){
          tiles[i][j].render();
        }
      }
      //render hole
      hole.render();
      tee.render();
      //render aimline
      drawAimLine();
      //render ball
      ball.render();
      //render swing bar
      if(shotState == 2){
        swingBar.render();
      }
      //tabmap
      if(tab){
        rectMode(CORNER);
        fill(128,128);
        rect(0,0,width,height);
        renderTabMap();
      }
}

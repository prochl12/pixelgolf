///////////////////////////////////////////////////////
//                   /*GOLF v01*/                    //
//                                                   //
//  /* Ball and tiles + generation */                //
//  /* Ball movement using arrow keys */             //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v02*/                    //
//  /* Mouse control support implemented */          //
//  /* Aim line added*/                              //
//  /* Aiming and powering modes added */            //
//     - click LMB to advance mode, RMB to cancel    //
//  /* Ball's direction vector added */              //
//     - creates automatically when advancing from   //
//       mode 1 (aim) to mode 2 (power)              //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v03*/                    //
//  /* Ball can now be stroked, howewer no club      //
//     types are present yet so now you              //
//     are only able to putt */                      //
//  /* Putting the ball out of map now gives         //
//     a blank color instead of graphic bugs*/       //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v04*/                    //
//  /* Let's try objects of Clubs :D */              //
//  /* Different clubs working!! */                  //
//  /* The WEDGE LIFTING THE BALL!!!!!!              //
//     AYYYYYYYY LMAOOO!!!!! */                      //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v05*/                    //
//  /* Textures updated */                           //
//  /* Ball position can now be altered by           //
//     differing the values of playerX,Y and         //
//     ball.x,y/*                                    //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v06*/                    //
//  /* Added hole class. One hole can be now         //
//     spawned by setting the hole's object          //
//     variables hole.x and hole.y*/                 //
//  /* Reworked ball flight. The ball now moves      //
//     2 times slower but also slowes down           //
//     2 times slower.                               //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v07*/                    //
//  /* The method Ball.isInHole(); returns           //
//     a boolean value saying if the ball's and      //
//     hole's coords do intersect */                 //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v08*/                    //
//  /* Map loading (see map class) although maps     //
//     are not working yet */                        //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v09*/                    //
//  /* Map loading working */                        //
//  /* Ball.toPin() method added */                  //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                  SECOND SEMESTER                  //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v10*/                    //
//  /* GUI implemented */                            //
//  /* TabMap added */                               //
//  /* Added Tee texture, class and Tee itself */    //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v11*/                    //
//  /* GUI tweak */                                  //
//  /* OB */                                         //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v12*/                    //
//  /* Ball movement rework */                       //
//  /* Map loading screen */                         //
//  /* Controllable stroke power! */                 //
//  /* OB tweaked, fixed */                          //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v13*/                    //
//  /* literally no changelog written */             //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v14*/                    //
//  /* Coords fixed!!! */                            //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v15*/                    //
//  /* Club selection GUI */                         //
//  /* Wind & GUI added */                           //
//  /* Flag removed when on green */                 //
//  /* Post-hole scorecard */                        //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v16*/                    //
//  /* Post shot rating */                           //
//  /* Wind strength optimized */                    //
//  /* Hole indicator fixed */                       //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                   /*GOLF v17*/                    //
//  /* Press ESC to bring up the Pause menu */       //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                   /*GOLF v18*/                    //
//  /* Whole course can be now played */             //
//                                                   //
//            ///////////////////////////            //
//                                                   //
//                                                   //
//                  /*GOLF v18b*/                    //
//  /* Let's play around with multiplayer */         //
//                                                   //
//            ///////////////////////////            //
//                                                   //
///////////////////////////////////////////////////////

//TODO LIST:

//PAUSE MENU

//IMPORTS
import controlP5.*;
import processing.net.*;

//CONSTANTS DECLARATION//////////////////////////////////////////
final int SCREEN_WIDTH = 1366;
final int SCREEN_HEIGHT = 768;

final int TILE_SIZE = 64;
final int BALL_SIZE = 10;

//MAP IDs
final int MAP_PAR     = 0;
final int MAP_TEE_X   = 1;
final int MAP_TEE_Y   = 2;
final int MAP_HOLE_X  = 3;
final int MAP_HOLE_Y  = 4;
//TILE IDs
final int OB        = 0;
final int WATER     = 1;
final int GRASS     = 2;
final int GREEN     = 3;
final int BUSH      = 4;
final int ROUGH     = 5;
final int BUNKER    = 6;
//CLUB TYPE IDs
final int DRIVER = 0;
final int WEDGE = 1;
final int PUTTER = 2;

final int OFF = 0;
final int AIM = 1;
final int POWER = 2;
final int MOVING = 3;

//STATES
  final int TITLE = 0;
  final int MAIN = 1;
  final int MAPSELECT = 2;
  final int INGAME = 3;
  final int HOLESELECT = 4;
  final int HOLEDONE = 5;
  final int COURSESELECT = 6;
  final int ESCMENU = 7;
  final int OUT = 8;
  final int MESSAGE = 9;
  final int COURSEHOLEDONE = 10;
  final int COURSEDONE = 11;
  final int PLAYERCONFIG = 12;
  final int MULTIPLAYER = 13;
  final int LOBBY = 14;

//KEYBOARD VARIABLES DECLARATION
boolean left, right, up, down, space, enter, tab, esc;
//MOUSE VARIABLES DECLARATION
boolean lmb, rmb, mmb;

//GAME VARIABLES DECALRATION
ArrayList<PImage> tileImgs = new ArrayList<PImage>();
PImage greenIcon, ballImage;
PImage[] clubImgs = new PImage[3];

//Mutliplayer variables
Client client;

//CP5 VARIABLES
  PFont font1;
  PFont font2;
  
  PImage logo, spaceToStart;
  PImage plrImg, plrBg, plrFg, plrActive;
  float stsh, stsw, stsc;

//CP5 OBJECTS
  ControlFont cfont1;
  ControlFont cfont2;
  
  ScrollableList holeList, courseList, courseList2;
  
  Textfield playerName, serverIp;
  
  ControlP5 cp5title;
  ControlP5 cp5mainmenu;
  ControlP5 cp5ingame;
  ControlP5 cp5courseSelect;
  ControlP5 cp5holeSelect;
  ControlP5 cp5holeDone;
  ControlP5 cp5escMenu;
  ControlP5 cp5courseHoleDone;
  ControlP5 cp5courseDone;
  ControlP5 cp5player;
  ControlP5 cp5multiplayer;
  ControlP5 cp5lobby;

Map map;
Ball ball;
Hole hole;
int holeNum = 0;
int[] strokesField, parsField;
int strokesSum, parSum;
String[] maps;
Tee tee;
Wind wind;
PVector teeHole, ballHole;
Club[] clubs;
Tile[][] tiles;
SwingBar swingBar;
String scoreWord;
int ballR, ballG, ballB;

float viewX, viewY;

String[] courseNames, holeNames;

int state;
boolean showTabMap;

int mapSizeX, mapSizeY;

int shotState;
int selectedClub;

//SETUP////////////////////////////////////////////////////////////
void setup(){
//sketch setup
  //size(1250,600);
  fullScreen();
  background(0,169,0);
  viewX = 0;
  viewY = 0;
  greenIcon = loadImage("GreenIcon.png");
  ballImage = loadImage("ball.png");
  teeHole = new PVector();
  ballHole = new PVector();
  courseNames = listFolderNames(sketchPath()+"\\data\\maps");
  guiInit();
  loadPlayer();
  swingBar = new SwingBar();
  ball = new Ball();
  state = TITLE;
  //tile images init
  for(int i=0;i<7;i++){
    tileImgs.add(loadImage("tiles\\"+i+".png"));
  }
  //club images init
  for(int i=0;i<3;i++){
    clubImgs[i] = loadImage("clubs\\"+i+".png");
  }
  //clubs init
  selectedClub=0;
  clubs = new Club[]{
    new Club("Driver",     250, 0.3, 4000, DRIVER),
    new Club("AWD",     200, 0.3, 3000, DRIVER),
    new Club("Wedge 150",    150, 0.5,  2800, WEDGE),
    new Club("Wedge 120",    120, 0.5,  2000, WEDGE),  
    new Club("Wedge 80",     80, 0.5,  1400, WEDGE),
    new Club("Wedge 50",    80, 0.5,  1200, WEDGE),
    new Club("Wedge 30",    30, 0.6,  700, WEDGE),
    new Club("Putter 3",    150, 0.01, 1200, PUTTER),
    new Club("Putter 2",    50, 0.01, 480, PUTTER),
    new Club("Putter",      30, 0.01, 280, PUTTER),
    new Club("MiniPutter",   7, 0.01, 70, PUTTER),
  };
}
//</setup>

//DRAW/////////////////////////////////////////////////////////////////
void draw(){
  if(state==OUT){
    message("Out of bounds!!");
  }else if(state==MESSAGE){
    message(ball.rateShot());
  }else if(state==INGAME){
      inGameRender();
  }
    drawGui();
  
}
//</draw>

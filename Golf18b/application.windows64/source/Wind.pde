class Wind{
  
  PVector dir, normDir, newDir, arr1, arr2;
  int strength;
  
  Wind(){
    strength = 5000;
    this.dir = new PVector(
      random(-10, 10)/strength,
      random(-10, 10)/strength
    );
    this.normDir = new PVector();
    this.newDir = new PVector();
    this.arr1 = new PVector();
    this.arr2 = new PVector();
  }
  
  void render(){
    //UPDATE
    if(dir.x>newDir.x+0.01){
      dir.x-=0.000001;
    }else if(dir.x<newDir.x-0.01){
      dir.x+=0.000001;
    }
    if(dir.y>newDir.y+0.01){
      dir.y-=0.000001;
    }else if(dir.y<newDir.y-0.01){
      dir.y+=0.000001;
    }
    if(abs(dir.x-newDir.x)<0.0001&&abs(dir.y-newDir.y)<0.0001){
      this.update();
    }
    this.dir.normalize(normDir);
    this.arr1.set(normDir);
    arr1.rotate(PI/3*4);
    this.arr2.set(normDir);
    arr2.rotate(PI/3*2);
    //DRAW
    if(state==INGAME){
      stroke(255);
      strokeWeight(5);
      line(50-normDir.x*40, 500-this.normDir.y*40, 50+normDir.x*40, 500+this.normDir.y*40);
      line(50+normDir.x*40, 500+this.normDir.y*40, 50+normDir.x*40+arr1.x*25, 500+this.normDir.y*40+arr1.y*25);
      line(50+normDir.x*40, 500+this.normDir.y*40, 50+normDir.x*40+arr2.x*25, 500+this.normDir.y*40+arr2.y*25);
      //println(normDir);
      textAlign(LEFT, CENTER);
      //println(dist(0,0,dir.x, dir.y));
      text((float)(round((dist(0,0,dir.x, dir.y)*1000000)))/100+"px/h", 100, 500);
    }
  }
  
  void update(){
    this.newDir = new PVector(
      random(-10, 10)/strength,
      random(-10, 10)/strength
    );
  }
  
}

class Hole{
  
  int x,y;
  PImage img, img2;
  boolean flag;
  float distance;
  
  Hole(int tempX, int tempY){
    this.img = loadImage("hole2.png");
    this.img2 = loadImage("flag.png");
    flag = true;
    this.x = tempX;
    this.y = tempY;
  }
  
  void render(){
    if(this.x!=-1&&this.y!=-1){
      imageMode(CORNER);
      image(this.img,
            width/2+(this.x-0.5)*TILE_SIZE-ball.x-viewX,
            height/2+(this.y-1.5)*TILE_SIZE-ball.y-viewY
      );
    }
  }
  
  void renderFlag(){
    if(this.flag){
      imageMode(CORNER);
      image(this.img2,
            width/2+(this.x-0.5)*TILE_SIZE-ball.x-viewX,
            height/2+(this.y-1.5)*TILE_SIZE-ball.y-viewY
      );
    }
  }
  
  void renderTabMap(){
    if(this.x!=-1&&this.y!=-1){
      imageMode(CORNER);
      image(this.img,
            map.tabMapOffX+(map.holeVector.x*map.tabMapTileSize-map.tabMapTileSize*3),
            map.tabMapOffY+(map.holeVector.y*map.tabMapTileSize-map.tabMapTileSize*6),
            map.tabMapTileSize*3,//w
            map.tabMapTileSize*6//h
      );
      image(this.img2,
            map.tabMapOffX+(map.holeVector.x*map.tabMapTileSize-map.tabMapTileSize*3),
            map.tabMapOffY+(map.holeVector.y*map.tabMapTileSize-map.tabMapTileSize*6),
            map.tabMapTileSize*3,//w
            map.tabMapTileSize*6//h
      );
    }
  }
  
}

class Tee{
  
  float x1,y1,x2,y2;
  PImage img;
  
  Tee(int tempX, int tempY){
    this.img = loadImage("tee.png");
    teeHole.set(hole.x-tempX, hole.y-tempY);
    teeHole.normalize();
    x1 = tempX + 0.5*teeHole.x + 1*teeHole.y;
    y1 = tempY + 0.5*teeHole.y - 1*teeHole.x;
    x2 = tempX + 0.5*teeHole.x - 1*teeHole.y;
    y2 = tempY + 0.5*teeHole.y + 1*teeHole.x;
  }
  
  void render(){
    imageMode(CENTER);
    image(this.img,
          width/2+(this.x1)*TILE_SIZE-ball.x-viewX,
          height/2+(this.y1)*TILE_SIZE-ball.y-viewY
    );
    image(this.img,
          width/2+(this.x2)*TILE_SIZE-ball.x-viewX,
          height/2+(this.y2)*TILE_SIZE-ball.y-viewY
    );
  }
  
}

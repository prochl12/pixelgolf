void guiInit(){
  //IMAGE STUFF
  logo = loadImage("logo.png");
  spaceToStart = loadImage("spaceToStart.png");
  plrImg = loadImage("plrImg.png");
  plrBg = loadImage("plrBg.png");
  plrFg = loadImage("plrFg.png");
  plrActive = loadImage("plrActive.png");
  stsw = 415;
  stsh = 32;
  stsc = -1;
  
  //FONTS
  font1 = createFont("minecraft.ttf", 64);
  font2 = createFont("glassgauge.ttf", 32);
  cfont1 = new ControlFont(font1);
  cfont2 = new ControlFont(font2);
  
  //CP5s
  //  >TITLE<
  cp5title = new ControlP5(this, font2);
  
  //  >MAIN MENU<
  cp5mainmenu = new ControlP5(this, font2);
  cp5mainmenu.addButton("play hole")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width/2-150, height/2)
    .setSize(300,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5mainmenu.addButton("play course")
    .setBroadcast(false)
    .setValue(2)
    .setBroadcast(true)
    .setPosition(width/2-150, height/2+90)
    .setSize(300,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5mainmenu.addButton("multiplayer")
    .setBroadcast(false)
    .setValue(4)
    .setBroadcast(true)
    .setPosition(width/2-150, height/2+180)
    .setSize(300,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5mainmenu.addButton("player")
    .setBroadcast(false)
    .setValue(3)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setImages(plrBg, plrFg, plrActive)
    .setSize(64,64)
  ;
  
  //  >HOLE SELECT<
  cp5holeSelect = new ControlP5(this, font2);
  courseList = new ScrollableList(cp5holeSelect,"courses")
    .close()
    .setPosition(100, 130)
    .setSize(300, 400)
    .setBarHeight(70)
    .setItemHeight(70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
    .addItems(courseNames)
  ;
  holeList = new ScrollableList(cp5holeSelect,"holes")
    .close()
    .setPosition(width/2-150, 130)
    .setSize(300, 400)
    .setBarHeight(70)
    .setItemHeight(70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5holeSelect.addButton("back")
    .setLabel("<")
    .setBroadcast(false)
    .setValue(0.5)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(64,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5holeSelect.addButton("load")
    .setLabel("load >")
    .setBroadcast(false)
    .setValue(1.5)
    .setBroadcast(true)
    .setPosition(width-40-180, height-64-40)
    .setSize(180,64)
    .setColorBackground(color(150, 150))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
    .lock()
  ;
  //  >COURSE SELECT<
  cp5courseSelect = new ControlP5(this, font2);
  courseList2 = new ScrollableList(cp5courseSelect,"courses")
    .setLabel("courses")
    .close()
    .setPosition(100, 130)
    .setSize(300, 400)
    .setBarHeight(70)
    .setItemHeight(70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
    .addItems(courseNames)
  ;
  cp5courseSelect.addButton("back")
    .setLabel("<")
    .setBroadcast(false)
    .setValue(0.5)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(64,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5courseSelect.addButton("load")
    .setLabel("load >")
    .setBroadcast(false)
    .setValue(1.5)
    .setBroadcast(true)
    .setPosition(width-40-180, height-64-40)
    .setSize(180,64)
    .setColorBackground(color(150, 150))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
    .lock()
  ;
  
  //  >INGAME<
  cp5ingame = new ControlP5(this, font2);
  
  //  >HOLE DONE<
  cp5holeDone = new ControlP5(this, font2);
  cp5holeDone.addButton("back to menu")
    .setLabel("< menu")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5holeDone.addButton("replay")
    .setLabel("play again >")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width-256-40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  //  >COURSE HOLE DONE<
  cp5courseHoleDone = new ControlP5(this, font2);
  cp5courseHoleDone.addButton("back to menu")
    .setLabel("< menu")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5courseHoleDone.addButton("next hole")
    .setLabel("next hole >")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width-256-40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  //  >COURSE DONE<
  cp5courseDone = new ControlP5(this, font2);
  cp5courseDone.addButton("back to menu")
    .setLabel("< menu")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5courseDone.addButton("play again")
    .setLabel("play again >")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width-256-40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  //  >ESC MENU<
  cp5escMenu = new ControlP5(this, font2);
  cp5escMenu.addButton("back to game")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(width/2-(width/3-30)/2, height/2-65)
    .setSize(width/3-30,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5escMenu.addButton("replay")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width/2-(width/3-30)/2, height/2+15)
    .setSize(width/3-30,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5escMenu.addButton("menu")
    .setBroadcast(false)
    .setValue(2)
    .setBroadcast(true)
    .setPosition(width/2-(width/3-30)/2, height/2+90)
    .setSize(width/3-30,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  
  // >PLAYER CONFIG<
  cp5player = new ControlP5(this, font2);
  playerName = new Textfield(cp5player, "playerName")
     .setLabel("")
     .setPosition(width/8,200)
     .setSize(width/8*6,70)
     .setColor(color(255))
     .setColorCursor(color(255))
     .setColorForeground(color(0,150,0))
     .setColorBackground(color(0,200,0))
     .setValue("player")
     .align(-1, -1, -1, -1)
  ;
  cp5player.addSlider("red")
           .setLabel("")
           .setMax(255)
           .setValue(255)
           .setSize(450, 60)
           .setPosition(width/2-300, 300)
           .setColorBackground(color(150,0,0))
           .setColorForeground(color(255,0,0))
           .setColorActive(color(255,0,0))
  ;
  cp5player.addSlider("green")
           .setLabel("")
           .setMax(255)
           .setValue(255)
           .setSize(450, 60)
           .setPosition(width/2-300, 370)
           .setColorBackground(color(0,150,0))
           .setColorForeground(color(0,255,0))
           .setColorActive(color(0,255,0))
  ;
  cp5player.addSlider("blue")
           .setLabel("")
           .setMax(255)
           .setValue(255)
           .setSize(450, 60)
           .setPosition(width/2-300, 440)
           .setColorBackground(color(0,0,150))
           .setColorForeground(color(0,0,255))
           .setColorActive(color(0,0,255))
  ;
  cp5player.addButton("plrSave")
    .setLabel("< save")
    .setBroadcast(false)
    .setValue(256)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(256,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  //  >MULTIPLAYER<
  cp5multiplayer = new ControlP5(this, font2);
  serverIp = new Textfield(cp5multiplayer, "serverIp")
     .setLabel("")
     .setPosition(width/8,200)
     .setSize(width/8*6,70)
     .setColor(color(255))
     .setColorCursor(color(255))
     .setColorForeground(color(0,150,0))
     .setColorBackground(color(0,200,0))
     .setValue("")
  ;
  cp5multiplayer.addButton("connect")
    .setBroadcast(false)
    .setValue(1)
    .setBroadcast(true)
    .setPosition(width/2-150, height/2)
    .setSize(300,70)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  cp5multiplayer.addButton("back")
    .setLabel("<")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(64,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
  
  // >PLAYER CONFIG<
  cp5lobby = new ControlP5(this, font2);
  cp5lobby.addButton("disconnect")
    .setLabel("<")
    .setBroadcast(false)
    .setValue(0)
    .setBroadcast(true)
    .setPosition(40, height-64-40)
    .setSize(64,64)
    .setColorBackground(color(0,150,0))
    .setColorForeground(color(0,200,0))
    .setColorActive(color(0,255,0))
  ;
}

//--//--//--//--//--//--//--//--//--//--//--//--//--//--//
//--//--//--//--//--//--//--//--//--//--//--//--//--//--//
//--//--//--//--//--//--//--//--//--//--//--//--//--//--//

void drawGui(){
  //  >TITLE<
    if(state==TITLE){
      if(enter||space){
        state=MAIN;
      }
      background(0,169,0);
      cp5title.show();
      imageMode(CENTER);
      image(logo,width/2,height/2);
      if(stsw>415||stsw<350){
        stsc *= -1;
      }
      stsh += (stsc*0.25);
      stsw += (stsc*13.97*0.25);
      image(spaceToStart, width/2+logo.width/3, height/2+logo.height/2, stsw, stsh);
    }else{
      cp5title.hide();
    }
  
  //  >MAIN MENU<
    if(state==MAIN){
      background(0,169,0);
      cp5mainmenu.show();
      imageMode(CENTER);
      image(logo,width/2,height/4, 350, 214);
      imageMode(CORNER);
      image(plrImg, 40, height-64-40, 64, 64);
    }else{
      cp5mainmenu.hide();
    }
    
  //  >HOLE SELECT<
    if(state==HOLESELECT){
      background(0,169,0);
      cp5holeSelect.show();
      fill(0,150,0);
      noStroke();
      rectMode(CORNER);
      rect(100, 40, 350, 70);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("HOLE SELECT", 105, 45);
    }else{
      cp5holeSelect.hide();
    }
  //  >COURSE SELECT<
    if(state==COURSESELECT){
      background(0,169,0);
      cp5courseSelect.show();
      fill(0,150,0);
      noStroke();
      rectMode(CORNER);
      rect(100, 40, 350, 70);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("COURSE SELECT", 105, 45);
    }else{
      cp5courseSelect.hide();
    }
    
  //  >INGAME<
    if(state==INGAME){
      if(esc){
        state=ESCMENU;
      }
      cp5ingame.show();
      if(shotState==0&&mmb){
        viewX += (float)((mouseX - width/2))/width*64;
        viewY += (float)((mouseY - height/2))/height*64;
      }else{
        viewX = 0;
        viewY = 0;
      }
      ballHole.set(TILE_SIZE*hole.x-ball.x-viewX, TILE_SIZE*hole.y-ball.y-viewY);
      if(abs(ballHole.x)>width/2||abs(ballHole.y)>height/2){
        if(abs(ballHole.x)/(width/2-50)>abs(ballHole.y)/(height/2-50)){
          ballHole.mult((width/2-50)/abs(ballHole.x));
          if(ballHole.x>0){
            //ballHole.x-=50;
          }else{
            //ballHole.x+=50;
          }
        }else{
          ballHole.mult((height/2-50)/abs(ballHole.y));
          if(ballHole.y>0){
            //ballHole.y-=50;
          }else{
            //ballHole.y+=50;
          }
        }
        imageMode(CENTER);
        image(greenIcon, width/2+ballHole.x, height/2+ballHole.y);
      }
      fill(0,150,0);
      noStroke();
     // rect(0, 0, 350, 70, 0, 10, 10, 0);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("TO PIN: "+(float)(round((ball.toPin())*100))/100+"px", 5, 5);
      textAlign(LEFT, BOTTOM);
      text("PAR "+map.mapInfo[0], 5, height-5);
      text((float)((int)(hole.distance*100))/100+"px", 105, height-5);
      text("SHOT DIST: "+(float)((int)(ball.shotDistance*100))/100+"px", 300, height-5);
      textAlign(RIGHT, BOTTOM);
      text("STROKES "+ball.strokes, width-5, height-5);
      imageMode(CORNER);
      image(clubImgs[clubs[selectedClub].clubType], width-128, 0);
      textAlign(RIGHT, CENTER);
      text(clubs[selectedClub].name+" ("+clubs[selectedClub].distance+"px)", width-128-10, 64);
      //render wind
      wind.render();
    }else{
      cp5ingame.hide();
    }
  
  //  >HOLE DONE<
    if(state==HOLEDONE){
      //background(0,169,0,1);
      cp5holeDone.show();
      fill(218,181,121);
      stroke(109, 90, 60);
      strokeWeight(1);
      rect(100, 170, width-200, height-340);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("SCORECARD", 115, 205);
      textAlign(CENTER, CENTER);
      text("PAR: "+map.par+"    STROKES: "+ball.strokes+"    "+wordScore(ball.strokes,map.par),width/2, height/2);
    }else{
      cp5holeDone.hide();
    }
  //  >COURSE HOLE DONE<
    if(state==COURSEHOLEDONE){
      //background(0,169,0,1);
      cp5courseHoleDone.show();
      drawCourseScoreCard();
    }else{
      cp5courseHoleDone.hide();
    }
  //  >COURSE DONE<
    if(state==COURSEDONE){
      cp5courseDone.show();
      drawCourseScoreCard();
    }else{
      cp5courseDone.hide();
    }
  //  >ESC MENU<
    if(state==ESCMENU){
      cp5escMenu.show();
      stroke(255);
      strokeWeight(1);
      fill(0, 169, 0);
      rectMode(CENTER);
      rect(width/2, height/2, width/3, height/3*2);
      fill(255);
      strokeWeight(2);
      textFont(font2);
      textAlign(CENTER, CENTER);
      text("PAUSED",width/2,height/4+10);
    }else{
      cp5escMenu.hide();
    }
    if(state==PLAYERCONFIG){
      ballR = int(cp5player.getController("red").getValue());
      ballG = int(cp5player.getController("green").getValue());
      ballB = int(cp5player.getController("blue").getValue());
      background(0,169,0);
      cp5player.show();
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("PLAYER CONFIG", 105, 45);
      imageMode(CENTER);
      tint(ballR, ballG, ballB);
      image(ballImage, width/2+300-ballImage.width/2, 370);
      noTint();
    }else{
      cp5player.hide();
    }
    //  >MP CONNECT SCREEN<
    if(state==MULTIPLAYER){
      background(0,169,0);
      cp5multiplayer.show();
    }else{
      cp5multiplayer.hide();
    }
    //  >MP LOBBY<
    if(state==LOBBY){
      background(0,169,0);
      cp5lobby.show();
      fill(0,150,0);
      noStroke();
      rectMode(CORNER);
      rect(100, 40, 350, 70);
      textFont(font2);
      fill(255);
      textAlign(LEFT, TOP);
      text("LOBBY", 105, 45);
    }else{
      cp5lobby.hide();
    }
}

void drawAimLine(){
  if (shotState==1||shotState==2){
    strokeWeight(5);
    stroke(0);
    line(ball.scopeX, ball.scopeY, width/2, height/2);
  }
}
